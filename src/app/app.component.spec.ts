import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { AppModule } from './app.module';
import { GameComponent } from './game/game.component';
import { TerritoryHoverPanelComponent } from './territory-hover-panel/territory-hover-panel.component';
import { MapComponent } from './map/map.component';
import { GameAlertsComponent } from './game-alerts/game-alerts.component';
import { AppRoutingModule } from './app-routing.module';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        MapComponent,
        GameComponent,
        TerritoryHoverPanelComponent,
        GameAlertsComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

});
