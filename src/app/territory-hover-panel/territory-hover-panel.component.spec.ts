import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerritoryHoverPanelComponent } from './territory-hover-panel.component';
import { MapTerritory } from '../map/map-territory';
import { Vector2 } from 'three';

describe('TerritoryHoverPanelComponent', () => {
  let component: TerritoryHoverPanelComponent;
  let fixture: ComponentFixture<TerritoryHoverPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerritoryHoverPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerritoryHoverPanelComponent);
    component = fixture.componentInstance;
    component.targetTerritory = {
      index: new Vector2(0, 0),
      landscapeType: 'grassland',
      villageType: 'unknown',
      villageLevel: 0,
      producesTroop: null,
      troopHr: 0,
      goldHr: 0,
      troops: [],
      getTroopsNameAmount: null,
      owner: {id: 545, username: 'someplay', isPlayer: false}
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
