import { Component, OnInit, Input } from '@angular/core';
import { MapTerritory } from '../map/map-territory';
import { ResourceService } from '../game/resource-service';
import { MapService } from '../map/map.service';

@Component({
  selector: 'app-territory-hover-panel',
  templateUrl: './territory-hover-panel.component.html',
  styleUrls: ['./territory-hover-panel.component.css']
})
export class TerritoryHoverPanelComponent implements OnInit {
  @Input() targetTerritory: MapTerritory;

  constructor(private resources: ResourceService, private mapService: MapService) { }

  ngOnInit() {
  }
  
  formattedIndex() {
    return this.targetTerritory.getFormattedCoordinates(this.mapService);
  }

  getLandscapeColor(): string {
    return this.resources.getTerritoryLandscapeColor(this.targetTerritory.landscapeType);
  }

  getOwner() : string {
    if(this.targetTerritory.owner == null)
      return 'unknown';
    if(this.targetTerritory.owner.username == null)
      return 'unknown';
    return this.targetTerritory.owner.username;
  }
}
