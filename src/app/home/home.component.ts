import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onPlay() {
    if(this.isAuthenticated())
      this.router.navigate(['/', 'play']);
    else 
      this.router.navigate(['/', 'signin'], { queryParams: {'redirectTo' : ['/', 'play']} });
  }

  onSignIn() {
    this.router.navigate(['/', 'signin']);
  }

  isAuthenticated() : boolean {
    return this.authService.isAuthenticated();
  }
}
