import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../auth/auth-service';
import { AlertsService } from '../alerts/alerts-service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  constructor(private authService: AuthService, private alerts: AlertsService) { }

  @Input() territoryListOpenned: boolean;
  @Output() onTerritoryListClick = new EventEmitter<void>();

  ngOnInit() {
  }

  getUsername(): string {
    return this.authService.getUsername();
  }

  onTerritoriesClick() {
    this.onTerritoryListClick.emit();
  }

  onResearchClick() {
    this.alerts.pushSystemAlert("Not implemeted!", "Reseach not yet implemented!");
  }

  onLeaderboardsClick() {
    this.alerts.pushSystemAlert("Not implemeted!", "Leaderboard not yet implemented!");
  }
}
