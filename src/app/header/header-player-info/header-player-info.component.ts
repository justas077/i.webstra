import { Component, OnInit, OnDestroy } from '@angular/core';
import { PlayerService, PlayerInformation } from 'src/app/auth/player.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header-player-info',
  templateUrl: './header-player-info.component.html',
  styleUrls: ['./header-player-info.component.css']
})
export class HeaderPlayerInfoComponent implements OnInit, OnDestroy {
  private infoSub: Subscription = Subscription.EMPTY;

  information: PlayerInformation;

  constructor(private playerService: PlayerService) { }

  ngOnInit() {
    this.information = this.playerService.getInformation();
    this.infoSub = this.playerService.playerInformationChanged.subscribe(
      (info) => {
        this.information = info;
      }
    );
  }

  ngOnDestroy() {
    this.infoSub.unsubscribe();
  }
}
