import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderPlayerInfoComponent } from './header-player-info.component';

describe('HeaderPlayerInfoComponent', () => {
  let component: HeaderPlayerInfoComponent;
  let fixture: ComponentFixture<HeaderPlayerInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderPlayerInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderPlayerInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
