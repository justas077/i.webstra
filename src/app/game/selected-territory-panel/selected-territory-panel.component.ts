import { Component, OnInit, Input, OnDestroy, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { MapTerritory } from 'src/app/map/map-territory';
import { ResourceService } from '../resource-service';
import { ConfigurationService } from '../configuration-service';
import { MapService } from 'src/app/map/map.service';
import { Subscription, timer } from 'rxjs';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { TimeService } from '../time.service';
import * as moment from 'moment';
import { AlertsService } from 'src/app/alerts/alerts-service';

@Component({
  selector: 'app-selected-territory-panel',
  templateUrl: './selected-territory-panel.component.html',
  styleUrls: ['./selected-territory-panel.component.css']
})
export class SelectedTerritoryPanelComponent implements OnInit, OnDestroy {
  private tileClickSub: Subscription = Subscription.EMPTY;
  private timerSub: Subscription = Subscription.EMPTY;
  private fastTimerSub: Subscription = Subscription.EMPTY;
  private territoryUpdatedSub: Subscription = Subscription.EMPTY;
  private selectionResetSub: Subscription = Subscription.EMPTY;

  private _targetTerritory: MapTerritory;

  troopCountSelection: TroopCountSelection;
  stationedTroops: { name: string, type: string, amount: number } [];
  troopsForRelocation: { name: string, type: string, amount: number } [] = null;
  incomingTroops: { name: string, timeLeft: number, amount: number } [] = null;

  coorsText: string;
  owner: string;
  ownedByPlayer: boolean;
  troopProductionText: string;
  troopProductionProgress: number = 0;

  landscapeTextColor: string;

  destinationSelectText: string = '';

  @ViewChild('troop_fill') troopFill: ElementRef;

  constructor(
    private resources: ResourceService, 
    private config: ConfigurationService, 
    private mapService: MapService,
    private time: TimeService,
    private http: HttpClient,
    private changeDetector: ChangeDetectorRef,
    private alerts: AlertsService) { }

  ngOnInit() {
    this.tileClickSub = this.mapService.onTileClicked.subscribe((t) => this.onTileClicked(t));

    this.territoryUpdatedSub = this.mapService.onTerritoryUpdated.subscribe(
      (territory) => { if(territory == this.targetTerritory) this.updateView() });

    this.timerSub = timer(0, 1000).subscribe( (n) => { this.timerTick(n) } );
    this.timerSub = timer(0, 50).subscribe( (n) => { this.fastTimerTick(n) } );
    this.selectionResetSub = this.mapService.getMapComponent().onSelectionReset.subscribe(
      () => this.targetTerritoryChanged() );
  }

  

  @Input() set targetTerritory(value: MapTerritory) {
    this._targetTerritory = value;
    this.targetTerritoryChanged();
  };
  get targetTerritory(): MapTerritory {
    return this._targetTerritory;
  }

  private targetTerritoryChanged() {
    this.troopCountSelection = null;
    this.troopsForRelocation = null;
    this.destinationSelectText = '';
    if(this.mapService.getMapComponent().isSelectingDestination())
      this.mapService.getMapComponent().destinationSelectionEnded();
    
    this.coorsText = this.targetTerritory.getFormattedCoordinates(this.mapService);
    this.landscapeTextColor = this.targetTerritory.getLandscapeTextColor(this.resources);

    this.updateView();
  }

  formTimeString(seconds: number): string {
    return moment(new Date(0, 0, 0, 0, 0, seconds, 0)).format('HH:mm:ss');
  }

  timerTick(n: number) {
    if(this.incomingTroops != null) {
      let evalIncoming = false;
      this.incomingTroops.forEach(t => {
        t.timeLeft--;
        if(t.timeLeft < 0) {
          evalIncoming = true;
          t.timeLeft = 0;
        }
        if(evalIncoming) {
          this.mapService.evaluateIncomingTroopsFor(this.targetTerritory);
        }
        this.changeDetector.detectChanges();
      });
    }
  }

  fastTimerTick(n: number) {
    this.updateTroopProductionProgress();
  }

  updateView() {
    this.incomingTroops = this.targetTerritory.getIncomingTroopArray(this.config.getConfiguration(), this.time);
    this.stationedTroops = this.targetTerritory.getTroopsTypeNameAmount(this.config.getConfiguration());
    
    this.troopProductionText = this.targetTerritory.getTroopProductionText(this.config.getConfiguration());

    this.owner = this.targetTerritory.getOwnerNameWhose();
    this.ownedByPlayer = this.targetTerritory.getOwnedByCurrentPlayer();

    if(this.troopCountSelection != null) {
      this.troopCountSelection.restructureTo(this.stationedTroops);
    }
    this.updateTroopProductionProgress();
  }

  private updateTroopProductionProgress() {
    if(this.troopFill == null) 
      return;
    const prog = this.mapService.getTroopProductionProgress(this.targetTerritory);
    this.troopFill.nativeElement.style['width'] = prog * 100 + '%';
  }

  onTileClicked(territory: MapTerritory) {
    if(this.troopsForRelocation != null) {
      if(territory == null || 
         territory.index.distanceToSquared(this.targetTerritory.index) != 1 ||
         territory.owner == null ||
         !territory.owner.isCurrentPlayer) {
        this.destinationSelectText = 'Invalid destination';
        return;
      }

      var body = { troops: { } };
      this.troopsForRelocation.forEach( t => body.troops[t.type] = t.amount );

      this.http.put(
        environment.apiUrl + 'troops/move/' + this.targetTerritory.id + '/' + territory.id,
        body).subscribe(
          (response) => {
            this.mapService.addIncomingTroopsFor(territory.index, response);
          },
          (error) => {
            this.alerts.pushSystemAlertReloadPage('Error while sending troops!');
            console.log(error);
          }
        );

      this.mapService.removeTroopsFrom(this.targetTerritory, body.troops);
      
      this.targetTerritory = this.targetTerritory; //Reapply territory, so the view can be reset
    }
  }

  submitTroopCount() {
    this.troopsForRelocation = [];

    this.troopCountSelection.entries.forEach(entry => {
      this.troopsForRelocation.push({
        name: entry.troopName,
        type: entry.troopType,
        amount: entry.control.value
      });
    });

    this.troopCountSelection = null;

    this.destinationSelectText = 'Please select the destination';
    this.mapService.getMapComponent().setupForDestinationSelection();
  }

  onSendTroops() {
    this.troopCountSelection = TroopCountSelection.createNew(this.stationedTroops);
  }

  onCancel() {
    if(this.troopsForRelocation != null) {
      this.mapService.getMapComponent().destinationSelectionEnded();
      this.destinationSelectText = '';
      this.troopsForRelocation = null;
    }
    if (this.troopCountSelection != null)
      this.troopCountSelection = null;
  }

  ngOnDestroy() {
    this.tileClickSub.unsubscribe();
    this.timerSub.unsubscribe();
    this.fastTimerSub.unsubscribe();
    this.territoryUpdatedSub.unsubscribe();
    this.selectionResetSub.unsubscribe();

    this.onCancel();
  }
}
class TroopCountSelection {
  entries: TroopSelectionEntry[] = [];
  form: FormGroup;

  static createNew(selectFrom: { name: string, type: string, amount: number } [])
    : TroopCountSelection 
  {
    const controls: FormControl[] = [];
    const entries: TroopSelectionEntry[] = [];
    selectFrom.forEach(t => {
      const entry: TroopSelectionEntry = new TroopSelectionEntry();
      entry.control = new FormControl(0, [Validators.min(0), Validators.max(t.amount), this.wholeNumberValidator]);
      entry.troopType = t.type;
      entry.max = t.amount;
      entry.troopName = t.name;
      entries.push(entry);
      controls.push(entry.control);
    });
    const result = new TroopCountSelection();
    result.entries = entries;
    result.form = new FormGroup({'troops': new FormArray(controls, this.anyTroops)});
    return result;
  }

  restructureTo(to: { name: string, type: string, amount: number }[]) {
    const newSelection = TroopCountSelection.createNew(to);

    this.entries.forEach(entry => {
      for(let newEntry of newSelection.entries) {
        if(newEntry.troopType == entry.troopType) {
          newEntry.control.reset(Math.min(entry.control.value, newEntry.max));
        }
      }
    });

    this.entries = newSelection.entries;
    this.form = newSelection.form;
  }

  getControl(i: number) {
    return (<FormArray>this.form.get('troops')).controls[i];
  }

  static anyTroops(array: FormArray): { [s: string]: boolean } {
    for (let count of array.value) {
      if (count > 0)
        return null;
    }
    return { 'noTroopsSelected': true };
  }

  static wholeNumberValidator(control: FormControl): { [s: string]: boolean } {
    if (control.value == Math.trunc(control.value))
      return null;
    return { 'notWhole': true };
  }
}

class TroopSelectionEntry {
  troopType: string;
  troopName: string;
  max: number;
  control: FormControl;
  inputElement;
}