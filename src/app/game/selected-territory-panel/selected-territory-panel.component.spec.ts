import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedTerritoryPanelComponent } from './selected-territory-panel.component';

describe('SelectedTerritoryPanelComponent', () => {
  let component: SelectedTerritoryPanelComponent;
  let fixture: ComponentFixture<SelectedTerritoryPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectedTerritoryPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedTerritoryPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
