import { Component, OnInit, OnDestroy, HostListener, ViewChild } from '@angular/core';
import { MapService } from '../map/map.service';
import { MapTerritory } from '../map/map-territory';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth-service';
import { Router } from '@angular/router';
import { ConfigurationService } from './configuration-service';
import { ResourceService } from './resource-service';
import { PlayerService } from '../auth/player.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit, OnDestroy {
  private configurationLoadSub: Subscription = Subscription.EMPTY;
  private resourceLoadSub: Subscription = Subscription.EMPTY;
  private mapInitialStateSub: Subscription = Subscription.EMPTY;
  private playerInformationSub: Subscription = Subscription.EMPTY;
  private territorySelectSub: Subscription = Subscription.EMPTY;
  private territoryHoverSub: Subscription = Subscription.EMPTY;

  hoveredTerritory: MapTerritory;
  selectedTerritory: MapTerritory;

  lastMessage: number = 0;

  territoryListOpenned: boolean = false;

  constructor(
    private mapService: MapService, 
    private configurationService: ConfigurationService,
    private resourceService: ResourceService,
    private authService: AuthService,
    private playerService: PlayerService,
    private router: Router) { }

  ngOnInit() {
    if(!this.authService.isAuthenticated()) {
      console.warn('Should be set up so not authenticated users dont get here!');
      this.router.navigate(['/', 'signin'], { queryParams: { redirectTo: ['/', 'play'] } });
      return;
    }
  
    this.territoryHoverSub = this.mapService.onTileHoverChanged.subscribe(
      (territory: MapTerritory) => this.hoveredTerritory = territory);

    this.territorySelectSub = this.mapService.onSelectedTileChanged.subscribe(
      (territory: MapTerritory) => this.selectedTerritory = territory);

    if(!this.configurationService.isLoaded() && this.configurationLoadSub.closed) {
      this.configurationLoadSub = this.configurationService.loadConfiguration().subscribe(
        () => {
          this.mapInitialStateSub = this.mapService.loadInitialState().subscribe();
        }
      );
    }

    if(!this.resourceService.isLoaded() && this.resourceLoadSub.closed) {
      this.resourceLoadSub = this.resourceService.loadResources().subscribe();
    }

    if(this.playerInformationSub.closed) {
      this.playerInformationSub = this.playerService.loadInformation().subscribe();
    }
  }

  territoryListBtnClick() {
    this.territoryListOpenned = !this.territoryListOpenned;
  }

  onReturnHome() {
    const homeTile = this.mapService.getDefaultTargetIndex();
    if(homeTile == null) {
      console.log("USER HAS NO TILES!");
      return;
    }

    this.mapService.getMapComponent().resetSelection();
    this.mapService.getMapComponent().setCameraTarget(homeTile);
    this.mapService.getMapComponent().selectTileAt(homeTile);
  }

  /**
   * @returns True, if the game is ready to be displayed for the player, false otherwise.
   */
  gameReady(): boolean {
    return this.authService.isAuthenticated() && 
      this.configurationService.isLoaded() && 
      this.resourceService.isLoaded() &&
      this.mapService.isInitialStateLoaded();
  }

  ngOnDestroy() {
    this.territoryHoverSub.unsubscribe();
    this.resourceLoadSub.unsubscribe();
    this.configurationLoadSub.unsubscribe();
    this.mapInitialStateSub.unsubscribe();
    this.playerInformationSub.unsubscribe();
    this.territorySelectSub.unsubscribe();
  }
}
