import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Vector2 } from 'three';
import { deepCopy } from '../shared/utilities';
import { environment } from 'src/environments/environment';

@Injectable()
export class ConfigurationService {
    private gameConfiguration: GameConfiguration = null;

    gameConfigurationChanged: Subject<GameConfiguration> = new Subject();
    mapConfigurationChanged: Subject<MapConfiguration> = new Subject();

    constructor(private http: HttpClient) {}

    /**
     * Attempts to load all the necessary configuration for the game.
     * @returns An observable, which either emits the copy of the resulted configuration and completes,
     * or fails and returns the error.
     */
    loadConfiguration() : Observable<GameConfiguration> {
        return this.http.get(environment.apiUrl + "configuration").pipe(
            map((result) => {
                //console.log('map', result);
                const config = new GameConfiguration();
                config.mapConfiguration = { 
                    size: new Vector2(result['map_size'], result['map_size']),
                    chunkSize: result['map_chunk_size'] 
                };
                config.natureTroops = {};
                config.playerTroops = {};
                config.industries = {};

                for (var property in result['nature_troops']) {
                    if (result['nature_troops'].hasOwnProperty(property)) {
                        const troop = result['nature_troops'][property];
                        config.natureTroops[property] = {
                            id: property,
                            name: troop['name'],
                            tribe: troop['tribe'],
                            cost: troop['cost'],
                            hp: troop['hp'],
                            power: troop['power']
                        }
                    }
                }

                for (var property in result['player_troops']) {
                    if (result['player_troops'].hasOwnProperty(property)) {
                        const troop = result['player_troops'][property];
                        config.playerTroops[property] = {
                            id: property,
                            name: troop['name'],
                            tribe: troop['tribe'],
                            cost: troop['cost'],
                            hp: troop['hp'],
                            power: troop['power']
                        }
                    }
                }



                this.gameConfiguration = config;
                //console.log(this.gameConfiguration);
                const copy = deepCopy(this.gameConfiguration);
                this.gameConfigurationChanged.next(copy);
                this.mapConfigurationChanged.next(copy.mapConfiguration);
                return copy;
            })
        );
    }

    /**
     * @returns True, if the configuration is loaded, false otherwise.
     */
    isLoaded() : boolean {
        return this.gameConfiguration != null;
    }

    /**
     * @returns A copy of the currently loaded configuration,
     * null if no configuration is loaded.
     */
    getConfiguration() : GameConfiguration {
        return deepCopy(this.gameConfiguration);
    }
}

export class GameConfiguration {
    mapConfiguration: MapConfiguration;
    playerTroops: { [id: string] : TroopConfiguration };
    natureTroops: { [id: string] : TroopConfiguration };
    industries: { [id: string] : IndustryConfiguration };
}

export class MapConfiguration {
    size: Vector2;
    chunkSize: number;
}

export class TroopConfiguration {
    id: string;
    name: string;
    tribe: string;

    cost: number;
    hp: number;
    power: number;
}

export class IndustryConfiguration {
    id: string;
    name: string;
}