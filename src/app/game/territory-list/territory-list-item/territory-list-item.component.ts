import { Component, OnInit, Input, HostListener } from '@angular/core';
import { Vector2 } from 'three';
import { MapService } from 'src/app/map/map.service';
import { AlertsService } from 'src/app/alerts/alerts-service';

@Component({
  selector: 'app-territory-list-item',
  templateUrl: './territory-list-item.component.html',
  styleUrls: ['./territory-list-item.component.css']
})
export class TerritoryListItemComponent implements OnInit {
  landscapeColors: { [id: string]: string } = {
    'grassland': '#254126',
    'forest': '#142f23',
    'fertile-land': '#2a1f18',
    'hill': '#011433',
    'swamp': '#113838',
    'desert': '#504728'
  };
  hrColors: { [id: string]: string } = {
    'grassland': '#26ae3b',
    'forest': '#2b642b',
    'fertile-land': '#805d3a',
    'hill': '#15c421',
    'swamp': '#29b081',
    'desert': '#c6ad67'
  };
  industryColors: { [id: string]: string } = {
    'transport': '#3f484a',
    'watchtower': '#61a2cc',
    'barracks': '#632c27',
    'factory': '#4a3c30',
    'stronghold': '#392854',
    'marketplace': '#7a6e2d',
    'blacksmith': '#333333',
    'siege': '#8c5135'
  };

  @Input() target: TerritoryListItemTarget;

  @HostListener('click', ['$event'])
  onEntryClick() {
    this.mapService.getMapComponent().resetSelection();
    this.mapService.getMapComponent().setCameraTarget(this.target.index);
    this.mapService.getMapComponent().selectTileAt(this.target.index);
  }

  constructor(private mapService: MapService, private alerts: AlertsService) { }

  ngOnInit() {
    
  }

  onUpgrade(event: MouseEvent) {
    event.stopImmediatePropagation();
    this.alerts.pushSystemAlert("Not implemented!", "Upgrade not yet implemented!");
  }

}
export class TerritoryListItemTarget {
  index: Vector2;
  coorsText: string;
  landscapeType: string;
  industryType: string;
  industryLevel: number;
}