import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerritoryListItemComponent } from './territory-list-item.component';

describe('TerritoryListItemComponent', () => {
  let component: TerritoryListItemComponent;
  let fixture: ComponentFixture<TerritoryListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerritoryListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerritoryListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
