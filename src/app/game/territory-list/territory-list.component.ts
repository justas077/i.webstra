import { Component, OnInit } from '@angular/core';
import { Vector2 } from 'three';
import { MapService } from '../../map/map.service';
import { TerritoryListItemTarget } from './territory-list-item/territory-list-item.component';

@Component({
  selector: 'app-territory-list',
  templateUrl: './territory-list.component.html',
  styleUrls: ['./territory-list.component.css']
})
export class TerritoryListComponent implements OnInit {

  entries: TerritoryListItemTarget[] = [];

  constructor(private mapService: MapService) { }

  ngOnInit() {
    this.entries = [];
    this.mapService.getUserTerritories().forEach(t => {
      
      this.entries.push(
        {
          index: t.index, 
          coorsText: t.getFormattedCoordinates(this.mapService),
          landscapeType: t.landscapeType,
          industryType: t.villageType,
          industryLevel: t.villageLevel
        }
        );
    });
  }
}
