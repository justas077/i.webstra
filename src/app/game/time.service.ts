import * as moment from 'moment-timezone';

export const TIME_DELAY: number = 0;

export class TimeService {
    // secondsTimer: Observable<number>;

    // constructor() {
    //     this.secondsTimer = timer(0, 1000);
    // }

    // getSecondsTimer(): Observable<number> {
    //     return this.secondsTimer;
    // }

    diffSeconds(from: Date, to: Date): number {
        
        return moment(from).diff(moment(to), 'seconds');
    }
    diffHours(from: Date, to: Date): number {
        return moment(to).diff(moment(from), 'hours', true);
    }

    diff(from: Date, to: Date): number {
        return moment(from).diff(moment(to));
    }

    dateFromBackend(date: string): Date {
        return moment.utc(date).toDate();
    }

    get now(): Date {
        return moment(Date.now()).subtract(TIME_DELAY, 'seconds').toDate();
    }
    get nowMoment(): moment.Moment {
        return moment(Date.now()).subtract(TIME_DELAY, 'seconds');
    }

    /**
     * @returns How many hours have pased since the given moment
     * @param m 
     */
    hoursFrom(m: moment.Moment): number {
        return this.diffHours(m.toDate(), this.now);
    }
}