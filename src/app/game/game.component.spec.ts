import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameComponent } from './game.component';
import { TerritoryHoverPanelComponent } from '../territory-hover-panel/territory-hover-panel.component';
import { MapComponent } from '../map/map.component';
import { MapService } from '../map/map.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { AuthService } from '../auth/auth-service';
import { GameAlertsComponent } from '../game-alerts/game-alerts.component';
import { RouterTestingModule } from '@angular/router/testing';
import { GameAlertsService } from '../game-alerts/game-alerts.service';
import { ConfigurationService } from './configuration-service';
import { SigninComponent } from '../signin/signin.component';
import { Router } from '@angular/router';

describe('GameComponent', () => {
  let component: GameComponent;
  let fixture: ComponentFixture<GameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        GameComponent, 
        TerritoryHoverPanelComponent, 
        MapComponent, 
        GameAlertsComponent,
        SigninComponent ],
      imports: [RouterTestingModule],
      providers: [ 
        MapService, 
        HttpClient, 
        AuthService, 
        HttpHandler, 
        GameAlertsService,
        ConfigurationService] 
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
