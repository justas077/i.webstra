import { Object3D, Mesh, BoxGeometry, BoxBufferGeometry, Texture, TextureLoader, MeshBasicMaterial, Material, PlaneBufferGeometry, MeshLambertMaterial, MeshStandardMaterial } from 'three';
import { Subject, Observable, observable, forkJoin } from 'rxjs';
import GLTFLoader from 'three-gltf-loader';
import { map, concat, toArray } from 'rxjs/operators';

export class ResourceService {

    resourcesLoaded: Subject<void> = new Subject();

    private loaded: boolean = false;

    //Resources--------------------------
    private landscapeColors: { [id: string] : string } = {
        'grassland' : 'chartreuse',
        'forest' : 'darkgreen',
        'fertile-land' : 'saddlebrown',
        'hill' : 'dimgrey',
        'swamp' : 'teal',
        'desert' : 'tan'
    };
    

    private baseGeometry: BoxBufferGeometry;
    private baseMaterial: Material;
    private baseMesh: Mesh;

    private territoryPlateGeometry: PlaneBufferGeometry;
    private territoryPlateMaterials: { [id: string] : Material } = {};
    private territoryPlateMesh: Mesh;

    private landscapeTextures: { [id: string] : Texture } = {};

    private borderTexture: Texture;
    private borderMaterials: { [id: string] : Material } = {};
    private borderMesh: Mesh;

    private terrainMeshes: { [id: string]: Mesh } = {};
    private terrainMaterials: { [id: string]: Material } = {};

    private industryMeshes: { [id: string] : Mesh[] } = {};


    newTerrainMesh(type: string): Mesh {
        if(this.terrainMeshes[type] == null)
            return null;
        return this.terrainMeshes[type].clone();
    }

    newIndustryMesh(type: string, level: number): Mesh {
        if (type == 'none' || this.industryMeshes[type] == null || level == null || level == 0)
            return null;
        if(this.industryMeshes[type].length == 0) {
            console.warn('No meshes of type ' + type + '. Returning placeholder');
            null;
        }
        
        let index = level - 1;
        if (index >= this.industryMeshes[type].length)
            index = this.industryMeshes[type].length-1;
        return this.industryMeshes[type][index].clone();
    }

    /**
     * Attempts to load all the necessary resources for the game.
     * @returns An observable, which either emits the copy of the resulted resources and completes,
     * or fails and returns the error.
     */
    loadResources() : Observable<void> {
        return this.loadTexturesAndMaterials().pipe(
            concat(forkJoin(this.createBasicAssets(), this.loadHill(), this.loadIndustryMeshes())),
            toArray()
        ).pipe(
            map(() => {
                this.loaded = true;
                this.resourcesLoaded.complete();
                return;
            })
        );
    }

    getTerritoryPlateMaterial(type: string) : Material {
        if(this.territoryPlateMaterials[type] != null)
            return this.territoryPlateMaterials[type];
        return this.territoryPlateMaterials['null'];
    }

    getBorderMaterial(type: string) : Material {
        if(this.borderMaterials[type] != null)
            return this.borderMaterials[type];
        return this.borderMaterials['none'];
    }

    newBaseMesh(): Mesh {
        return this.baseMesh.clone();
    }

    newBorder(): Mesh {
        return this.borderMesh.clone();
    }

    newTerritoryPlate(): Mesh {
        return this.territoryPlateMesh.clone();
    }

    private createBasicAssets() : Observable<void> {
        return new Observable(o => {
            this.baseGeometry = new BoxBufferGeometry( 1, 1, 1 );
            this.baseMaterial = new MeshBasicMaterial({ color: 0xa8702b } );
            this.baseMesh = new Mesh(this.baseGeometry, this.baseMaterial);

            this.territoryPlateGeometry = new PlaneBufferGeometry(1, 1);
            this.territoryPlateMesh = new Mesh(this.territoryPlateGeometry, this.territoryPlateMaterials['null']);

            this.borderMesh = new Mesh(this.territoryPlateGeometry, this.borderMaterials['none']);

            o.next();
            o.complete();
        });
    }

    private loadTexturesAndMaterials() : Observable<void> {
        const grassOb = new Observable( o => {
            new TextureLoader().load("assets/ground/grass.jpg",
                (texture) => {
                    this.landscapeTextures['grassland'] = texture;
                    o.next();
                    o.complete();
                })
        });
        const desertOb = new Observable( o => {
            new TextureLoader().load("assets/ground/desert.jpg",
                (texture) => {
                    this.landscapeTextures['desert'] = texture;
                    o.next();
                    o.complete();
                })
        });
        const swampOb = new Observable( o => {
            new TextureLoader().load("assets/ground/swamp.jpg",
                (texture) => {
                    this.landscapeTextures['swamp'] = texture;
                    o.next();
                    o.complete();
                })
        });
        const hillOb = new Observable( o => {
            new TextureLoader().load("assets/ground/hill.jpg",
                (texture) => {
                    this.landscapeTextures['hill'] = texture;
                    o.next();
                    o.complete();
                })
        });
        const fertile_landOb = new Observable( o => {
            new TextureLoader().load("assets/ground/fertile-land.jpg",
                (texture) => {
                    this.landscapeTextures['fertile-land'] = texture;
                    o.next();
                    o.complete();
                })
        });
        const forestOb = new Observable( o => {
            new TextureLoader().load("assets/ground/forest.jpg",
                (texture) => {
                    this.landscapeTextures['forest'] = texture;
                    o.next();
                    o.complete();
                })
        });
        const borderOb = new Observable( o => {
            new TextureLoader().load("assets/ground/border.png",
                (texture) => {
                    this.borderTexture = texture;
                    o.next();
                    o.complete();
                })
        });

        const hillMatOb = new Observable( o => {
            new TextureLoader().load("assets/terrain/hill_texture.png",
                (texture) => {
                    texture.flipY = false;
                    this.terrainMaterials['hill'] = new MeshLambertMaterial( { map: texture } );
                    o.next();
                    o.complete();
                })
        });

        const matOb = new Observable(o => {
            this.territoryPlateMaterials['grassland'] = new MeshLambertMaterial( { map: this.landscapeTextures['grassland'] } );
            this.territoryPlateMaterials['forest'] = new MeshLambertMaterial( { map: this.landscapeTextures['forest'] } );
            this.territoryPlateMaterials['fertile-land'] = new MeshLambertMaterial( { map: this.landscapeTextures['fertile-land'] } );
            this.territoryPlateMaterials['hill'] = new MeshLambertMaterial( { map: this.landscapeTextures['hill'] } );
            this.territoryPlateMaterials['swamp'] = new MeshLambertMaterial({ map: this.landscapeTextures['swamp'] });
            this.territoryPlateMaterials['desert'] = new MeshLambertMaterial( { map: this.landscapeTextures['desert'] } );

            this.territoryPlateMaterials['null'] = new MeshBasicMaterial({ color: 0x181818 });
            this.territoryPlateMaterials['hovered'] = new MeshLambertMaterial({ color: 0xd1af0a });

            this.borderMaterials['none'] = new MeshBasicMaterial( { transparent: true, opacity: 0 } );
            this.borderMaterials['owner'] = new MeshBasicMaterial({ map: this.borderTexture, transparent: true, color: 0x0075fc } );
            this.borderMaterials['enemy_player'] = new MeshBasicMaterial({ map: this.borderTexture, transparent: true, color: 0xef2759 } );

            o.next();
            o.complete();
        });

        return forkJoin(grassOb, borderOb, desertOb, swampOb, hillOb, forestOb, fertile_landOb, hillMatOb).pipe(
            concat(matOb),
            toArray()
        ).pipe( map( () => { return null } ) );
    }

    private loadIndustryMeshes() : Observable<void> {
        const gltfLoader = new GLTFLoader();
        return forkJoin(
            this.loadForts(gltfLoader), 
            this.loadSieges(gltfLoader))
            .pipe(
                map(() => { return null; })
        );
    }

    private loadForts(gltfLoader: GLTFLoader): Observable<void> {
        this.industryMeshes['stronghold'] = [];
        const fortOb = new Observable(o => {
            new TextureLoader().load("assets/industries/fort/fort_1.0.jpg",
                (texture) => {
                    gltfLoader.load('assets/industries/fort/fort_1.0.glb',
                        (result) => {
                            // console.log('fort', result);
                            const mesh: Mesh = <Mesh>result.scene.children[0];
                            mesh.material = this.createIndustryMaterial(texture);
                            this.industryMeshes['stronghold'].push(mesh);
                            o.next();
                            o.complete();
                        },
                        () => { },
                        (error) => {
                            o.error(error);
                        }
                    );
                })
        });

        return forkJoin(fortOb).pipe(
            map( () => { return null } )
        );
    }

    private loadSieges(gltfLoader: GLTFLoader): Observable<void> {
        this.industryMeshes['siege'] = [];
        const fortOb = new Observable(o => {
            new TextureLoader().load("assets/industries/siege/siege_1.0.jpg",
                (texture) => {
                    gltfLoader.load('assets/industries/siege/siege_1.0.gltf',
                        (result) => {
                            //console.log('siege', result);
                            const mesh: Mesh = <Mesh>result.scene.children[0];
                            mesh.material = this.createIndustryMaterial(texture);
                            this.industryMeshes['siege'].push(mesh);
                            o.next();
                            o.complete();
                        },
                        () => { },
                        (error) => {
                            o.error(error);
                        }
                    );
                })
        });

        return forkJoin(fortOb).pipe(
            map(() => { return null })
        );
    }

    private createIndustryMaterial(texture: Texture) {
        texture.flipY = false;
        return new MeshLambertMaterial({ map: texture, color: 0xcccccc} );
    }

    /**
     * @returns An observable, which loads the hill.
     */
    private loadHill(): Observable<void> {
        return new Observable(o => {
            const gltfLoader = new GLTFLoader();

            gltfLoader.load('assets/terrain/hill.glb',
                (result) => {
                    // console.log('hill', result);
                    //console.log(this.terrainMeshes['hill'].material);
                    //console.log(this.terrainMaterials['hill']);
                    this.terrainMeshes['hill'] = <Mesh>result.scene.children[2];
                    this.terrainMeshes['hill'].material = this.terrainMaterials['hill'];
                    this.terrainMeshes['hill'].rotation.set(Math.PI * 0.5, -Math.PI * 0.5, 0);
                    o.next();
                    o.complete();
                },
                () => { },
                (error) => {
                    o.error(error);
                }
            );
        });
    }

    getTerritoryLandscapeColor(landscapeType: string) : string {
        return this.landscapeColors[landscapeType];
    }

    /**
     * @returns True, if the resources are loaded, false otherwise.
     */
    isLoaded() : boolean {
        return this.loaded;
    }
}