import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { MapService } from './map/map.service';
import { TerritoryHoverPanelComponent } from './territory-hover-panel/territory-hover-panel.component';
import { GameComponent } from './game/game.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './auth/auth-service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { SigninComponent } from './signin/signin.component';
import { ConfigurationService } from './game/configuration-service';
import { ResourceService } from './game/resource-service';
import { AuthInterceptor } from './auth/auth.interceptor';
import { TerritoryListComponent } from './game/territory-list/territory-list.component';
import { HeaderComponent } from './header/header.component';
import { HeaderPlayerInfoComponent } from './header/header-player-info/header-player-info.component';
import { PlayerService } from './auth/player.service';
import { SelectedTerritoryPanelComponent } from './game/selected-territory-panel/selected-territory-panel.component';
import { TimeService } from './game/time.service';
import { TerritoryListItemComponent } from './game/territory-list/territory-list-item/territory-list-item.component';
import { AlertsComponent } from './alerts/alerts.component';
import { AlertComponent } from './alerts/alert/alert.component';
import { AlertsService } from './alerts/alerts-service';
import { SystemAlertContentComponent } from './alerts/alert/system-alert-content/system-alert-content.component';
import { UpgradeAlertContentComponent } from './alerts/alert/upgrade-alert-content/upgrade-alert-content.component';
import { AttackAlertContentComponent } from './alerts/alert/attack-alert-content/attack-alert-content.component';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    TerritoryHoverPanelComponent,
    GameComponent,
    HomeComponent,
    SigninComponent,
    TerritoryListComponent,
    HeaderComponent,
    HeaderPlayerInfoComponent,
    SelectedTerritoryPanelComponent,
    TerritoryListItemComponent,
    AlertsComponent,
    AlertComponent,
    SystemAlertContentComponent,
    UpgradeAlertContentComponent,
    AttackAlertContentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    MapService,
    AuthService,
    ConfigurationService,
    ResourceService,
    PlayerService,
    TimeService,
    AlertsService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
