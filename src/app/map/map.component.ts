import { Component, OnInit, ViewChild, HostListener, OnDestroy, ElementRef } from '@angular/core';
import { MapService } from './map.service';
import { Observable, Subject, Subscription } from 'rxjs';
import { Vector2, Mesh, Vector3, Raycaster, Scene, Clock, Color, AmbientLight, DirectionalLight, TextureLoader, AxesHelper, PlaneGeometry, MeshBasicMaterial, WebGLRenderer, OrthographicCamera, Object3D, GLTF, DirectionalLightHelper, MeshLambertMaterial, BasicShadowMap, MeshPhongMaterial, BoxGeometry, PointLight, PCFSoftShadowMap, PCFShadowMap } from 'three';
import { DisplayedTerritories } from './3d/displayed-map';
import { MapTile } from './3d/map-tile';
import { RenderTarget } from './3d/render-target';
import { OrthCameraSize } from './3d/rendering-utils';
import GLTFLoader from 'three-gltf-loader';
import { ResourceService } from '../game/resource-service';
import { MapTerritory } from './map-territory';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, OnDestroy {

  @ViewChild('mapContainer') mapContainer: ElementRef;
  @ViewChild('zoomSlider') zoomSlider: ElementRef;
  @ViewChild('state_text') state_text: ElementRef;

  private interactionsEnabledSub: Subscription;

  private focusedTileChanged: Subject<Vector2> = new Subject<Vector2>();
  private onUpdate: Subject<number> = new Subject<number>();
  onSelectionReset: Subject<void> = new Subject<void>();

  private CAMERA_SPEED = 0.6;
  private CAMERA_SPEED_MODIFIER = 5;
  private CAMERA_SPEED_DAMP = 8;

  private MIN_DRAG_DISTANCE = 0.2;

  private MIN_CAMERA_SIZE = 1;
  private MAX_CAMERA_SIZE = 10;

  private BACKROUND_COLOR = 0x2A2A2A;

  private cameraSize = 8;

  private renderTarget: RenderTarget;
  private camera: THREE.OrthographicCamera;
  private renderer: THREE.WebGLRenderer;
  private mapSidesCover: Mesh;
  private flashLight;

  private focusedCoordinates: Vector2;

  private targetCameraPosition: Vector3 = new Vector3();
  private currentCameraSpeed = new Vector3();

  private raycaster: Raycaster = new Raycaster();
  private hoveredIndex: Vector2;
  private selectedIndex: Vector2;

  private displayedTerritories: DisplayedTerritories;

  private mainScene: Scene;

  private clock = new Clock();

  private uiEvents;

  private tileSelectionEnabled: boolean = true;
  private dragEnabled: boolean = true;

  private selectingDestination: boolean = false;
  
  constructor(private mapService: MapService, private resourceService: ResourceService) {
    if(!this.resourceService.isLoaded())
      throw "Resources must be loaded before creating a map!"
   }

  ngOnInit() {
    this.renderTarget = new RenderTarget(this.mapContainer.nativeElement);

    this.mainScene = new Scene();

    this.displayedTerritories = new DisplayedTerritories(
      this, this.mainScene, this.mapService, this.resourceService);

    const camSize = this.calculateCameraSize(this.cameraSize, this.renderTarget.aspectRatio());
    this.camera = new OrthographicCamera(camSize.left, camSize.right,
      camSize.top, camSize.bottom, camSize.near, camSize.far);

    const targetPos = this.mapService.getDefaultTargetIndex();

    this.camera.position.set(targetPos.x, targetPos.y, 0);
    this.camera.rotation.set(Math.PI / 180 * 39, Math.PI / 180 * -32, Math.PI / 180 * 327);
    this.targetCameraPosition.copy(this.camera.position);

    this.displayedTerritories.setAreaPosition(targetPos.clone());
    this.displayedTerritories.initTiles();

    this.mapService.setMapCompoenent(this);

    this.setUpScene();

    this.renderer = new WebGLRenderer({ antialias: true });
    this.renderer.setSize(this.renderTarget.getWidth(), this.renderTarget.getHeight());

    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderTarget.getElement().appendChild(this.renderer.domElement);
    this.setInteractionsEnabled(this.mapService.areInteractionsEnabled());

    this.focusedTileChanged.subscribe((newIndex) => {
      this.mapService.focusedTerritoryChanged(newIndex.clone());
    });

    this.interactionsEnabledSub = this.mapService.interactionsEnabledChanged.subscribe(
      (enabled) => { this. setInteractionsEnabled(enabled); }
    );

    this.setZoom(0.75);

    this.renderer.setAnimationLoop(() => {
      this.update();
      this.render();
    })
    this.updateFocusedCoordinates();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.renderTargetResized();
  }

  @HostListener('document:keydown.P') 
  ondunno() {
    
  }  
  

  onZoomSliderValueChange(event) {
    this.setZoom(event.target.value * 0.01);
  }

  /**
   * Creates and sets up the main scene
   */
  private setUpScene() {

    this.mainScene.background = new Color(this.BACKROUND_COLOR);

    const ambientLight = new AmbientLight('white', 1.8);
    this.mainScene.add(ambientLight);

    const mapCoverTexture = new TextureLoader().load("assets/map-sides-cover.png");

    this.mapSidesCover = new Mesh(new PlaneGeometry(), new MeshBasicMaterial({ map: mapCoverTexture, transparent: true, depthTest: false}));
    this.mapSidesCover.renderOrder = 1;

    this.mainScene.add(this.mapSidesCover);

    const size = this.displayedTerritories.getSize() * 1.8;
    this.mapSidesCover.scale.set(size, size, 1);


    this.flashLight = new Object3D();
    // this.flashLight = new PointLight('white', 5, 12);
    // this.flashLight.position.set(0, 0, 10);
    // this.flashLight.castShadow = true;

    // this.flashLight.shadow.mapSize.set(2048, 2048);
    // this.flashLight.shadow.bias = 0.01;
    // this.flashLight.shadow.camera.far = 20;
    // this.flashLight.shadow.camera.visible = true;
    //console.log(this.flashLight.shadow.camera);
    

    this.mainScene.add(this.flashLight);
  }

  /**
   * Enables or disables the interactions with the map.
   * @param enabled Should the interactions be enabled
   */
  setInteractionsEnabled(enabled: boolean) {

    this.lastDragPos = null;
    this.pointerHeldDown = null;
    this.isDragging = null;

    if (enabled && this.uiEvents == null) {
      this.uiEvents = [
        { type: 'pointermove', handler: (event) => { this.onPointerMove(event) } },
        { type: 'pointerup', handler: (event) => { this.onPointerUp(event) } },
        { type: 'pointerdown', handler: (event) => { this.onPointerDown(event) } },
        { type: 'click', handler: (event) => { this.onClick(event) } },
        { type: 'pointerout', handler: (event) => { this.onPointerOut(event) } }
      ];
      this.uiEvents.forEach(event => {
        this.renderer.domElement.addEventListener(event.type, event.handler);
      });
    } else if (this.uiEvents != null) {
      this.uiEvents.forEach(event => {
        this.renderer.domElement.removeEventListener(event.type, event.handler);
      });
      this.uiEvents = null;
    }
  }

  //==EVENTS============================================

  onClick(event: MouseEvent) {
    if (this.hoveredIndex != null && this.displayedTerritories.getDisplayedTerritory(this.hoveredIndex) != null) {
      if (this.tileSelectionEnabled) {
        this.setSelectedTile(!this.compareNullableVector2(this.hoveredIndex, this.selectedIndex)  
          ? this.hoveredIndex 
          : null);
        
        // this.mapService.onSelectedTileChanged.next(
        //   this.selectedIndex == null ? null : this.mapService.getTerritory(this.selectedIndex.clone()))

        this.targetCameraPosition.set(this.hoveredIndex.x, this.hoveredIndex.y, 0);
        this.currentCameraSpeed.set(0, 0, 0);
      }

      this.mapService.onTileClicked.next(this.mapService.getTerritory(this.hoveredIndex));
    }
  }

  pointerHeldDown = false;
  isDragging = false;
  lastDragPos: PointerEvent;
  pointerDownPos: PointerEvent;

  onPointerMove(event: PointerEvent) {
    //console.log(event);
    this.updateHover(event);

    if (this.pointerHeldDown) {
      if (this.isDragging) {
        const dragPos = event;
        if (this.lastDragPos != null) {
          const diff2D = this.pointerToWorldPosition(this.lastDragPos).sub(this.pointerToWorldPosition(dragPos));
          const diff = new Vector3(diff2D.x, diff2D.y, 0);

          const newPos = new Vector3().copy(this.camera.position).add(diff);
          this.setCameraPosition(newPos, true);

          const deltaTime = (event.timeStamp - this.lastDragPos.timeStamp) / 1000;
          this.currentCameraSpeed = new Vector3().copy(diff).divideScalar(deltaTime);
        }
        this.lastDragPos = dragPos;
      } else if(this.dragEnabled) {
        const diff = this.pointerToWorldPosition(this.pointerDownPos).sub(this.pointerToWorldPosition(event));
        if (diff.lengthSq() > this.MIN_DRAG_DISTANCE * this.MIN_DRAG_DISTANCE) {
          this.isDragging = true;
          this.lastDragPos = event;
          this.currentCameraSpeed.set(0, 0, 0);
          if(this.selectedIndex != null)
            this.setSelectedTile(null);
        }
      }
    }
  }

  onPointerDown(event: PointerEvent) {
    this.pointerHeldDown = true;
    this.pointerDownPos = event;
  }

  onPointerUp(event: PointerEvent) {
    this.pointerHeldDown = false;

    if (this.isDragging) {
      const deltaTime = (event.timeStamp - this.lastDragPos.timeStamp) / 1000;

      const decay = this.CAMERA_SPEED_DAMP * deltaTime;
      if (this.currentCameraSpeed.lengthSq() < decay * decay) {
        this.currentCameraSpeed.set(0, 0, 0);
      } else {
        this.currentCameraSpeed.sub(new Vector3().copy(this.currentCameraSpeed).multiplyScalar(decay));
      }

      this.isDragging = false;

      this.ensureCameraTargetInsideMap(false);
    }
  }

  onPointerOut(event: PointerEvent) {
    if (this.isDragging)
      this.ensureCameraTargetInsideMap(false);

    this.pointerHeldDown = false;
    this.isDragging = false;
  }

  //==============================================

  updateHover(event: PointerEvent) {
    let newHover = this.isDragging ? null : this.pointerPositionToIndex(event);
    if (newHover != null && this.displayedTerritories.getDisplayedTerritory(newHover) == null)
      newHover = null;
    if(!this.compareNullableVector2(newHover, this.hoveredIndex))
      this.setHoveredTile(newHover);
  }

  /**
   * Compares two vectors, checking if they are null.
   * Consideres that they are the same if only one of them is null, or
   * both of the are not null and their position is the same.
   * @param v1 
   * @param v2 
   */
  compareNullableVector2(v1: Vector2, v2: Vector2): boolean {
    if(v1 == null && v2 == null)
      return true;
    if(v1 != null && v2 != null && v1.distanceToSquared(v2) == 0)
      return true;
    return false;
  }

  /**
   * Changes the hovered tile.
   * Does not check if the new hovered tile is different than the current one.
   * @param newHover The new hovered tile, null if no tile should be hovered over.
   */
  private setHoveredTile(newHover: Vector2) {
    this.hoveredIndex = newHover;
    this.mapService.onTileHoverChanged.next(this.hoveredIndex == null 
      ? null 
      : this.mapService.getTerritory(this.hoveredIndex));
  }

  private setSelectedTile(newSelected: Vector2) {
    this.selectedIndex = newSelected;
    this.mapService.onSelectedTileChanged.next(this.selectedIndex == null 
      ? null 
      : this.mapService.getTerritory(this.selectedIndex.clone()));
  }

  setupForDestinationSelection() {
    if(this.selectedIndex == null) {
      console.warn("No tiles are selected!");
      return;
    }
    const selectedTile = this.displayedTerritories.getDisplayedTile(this.selectedIndex);
    const selectedTerritory = this.displayedTerritories.getDisplayedTerritory(this.selectedIndex);
    if(selectedTerritory == null) {
      console.warn("The selected territory is null!");
      return;
    }

    this.selectingDestination = true;

    this.setTileSelectionEnabled(false);
    this.setDragEnabled(false);
    this.setCameraTarget(selectedTerritory.index);
      
    const raisedIndex = selectedTerritory.index;
    const territories: MapTerritory[] = [];

    territories.push(this.mapService.getTerritory(
      new Vector2(raisedIndex.x + 1, raisedIndex.y)));
    territories.push(this.mapService.getTerritory(
      new Vector2(raisedIndex.x - 1, raisedIndex.y)));
    territories.push(this.mapService.getTerritory(
      new Vector2(raisedIndex.x, raisedIndex.y + 1)));
    territories.push(this.mapService.getTerritory(
      new Vector2(raisedIndex.x, raisedIndex.y - 1)));

    const raisedIndexes = [];

    territories.forEach(t => {
      if(t != null && t.owner != null && t.owner.isCurrentPlayer) {
        raisedIndexes.push(t.index);
      }
    });
    this.displayedTerritories.setRaisedIndexes(raisedIndexes.slice());
  }

  destinationSelectionEnded() {
    this.selectingDestination = false;

    this.setTileSelectionEnabled(true);
    this.setDragEnabled(true);

    this.displayedTerritories.setRaisedIndexes(this.selectedIndex == null ? [] : [this.selectedIndex]);
  }

  isSelectingDestination() : boolean {
    return this.selectingDestination;
  }

  /**
   * Makes the camera position move to the closest point inside the map.
   * @param instant Whether the camera should move to the target instantly,
   * or over time.
   */
  ensureCameraTargetInsideMap(instant: boolean) {
    const size = this.mapService.getMapSize();
    const target = this.camera.position.clone();

    if (this.camera.position.x < 0)
      target.x = 0;
    else if (this.camera.position.x > size.x - 1)
      target.x = size.x - 1;

    if (this.camera.position.y < 0)
      target.y = 0;
    else if (this.camera.position.y > size.y - 1)
      target.y = size.y - 1;

    if (this.camera.position.x != target.x || this.camera.position.y != target.y) {
      if (instant)
        this.setCameraPosition(target.clone(), true);
      else
        this.targetCameraPosition = target.clone();
      this.currentCameraSpeed.set(0, 0, 0);
    }
  }

  setCameraTarget(index: Vector2) {
    this.targetCameraPosition.set(index.x, index.y, 0);
    this.currentCameraSpeed.set(0, 0, 0);
    this.isDragging = false;
    this.pointerHeldDown = false;
  }

  selectTileAt(index: Vector2) {
    this.setSelectedTile(index);
  }

  resetSelection() {
    this.isDragging = false;
    this.pointerHeldDown = false;
    this.setHoveredTile(null);
    if(this.selectingDestination)
      this.destinationSelectionEnded();
    this.setSelectedTile(null);
    this.onSelectionReset.next();
  }

  setCameraPosition(position: Vector3, overrideTarget = false) {
    this.camera.position.copy(position);
    if (overrideTarget)
      this.targetCameraPosition.copy(position);
    this.updateFocusedCoordinates();
  }

  updateFocusedCoordinates() {
    const newFocus = new Vector2(Math.round(this.camera.position.x), Math.round(this.camera.position.y));

    if (this.focusedCoordinates == null) {
      this.focusedCoordinates = newFocus;
      this.focusedTileChanged.next(newFocus.clone());
      this.displayedTerritories.setAreaCenterPosition(newFocus.clone().add(new Vector2(1, 1)));
    } else if (newFocus.distanceToSquared(this.focusedCoordinates) != 0) {
      this.displayedTerritories.setAreaCenterPosition(newFocus.clone().add(new Vector2(1, 1)));

      this.focusedCoordinates = newFocus;
      this.focusedTileChanged.next(newFocus.clone());
    }
  }

  private setTileSelectionEnabled(value: boolean) {
    if(this.tileSelectionEnabled == value)
      return;
    this.tileSelectionEnabled = value;
  }

  private setDragEnabled(value: boolean) {
    if(this.dragEnabled == value)
      return;
    this.dragEnabled = value;

    if(this.isDragging) {
      this.isDragging = false;
    }
  }

  private pointerPositionToTile(event: PointerEvent): MapTile {
    if (event == null)
      throw "Mouse event can't be null!";
    return this.worldPositionToTile(this.pointerToWorldPosition(event));
  }

  private pointerPositionToIndex(event: PointerEvent): Vector2 {
    if (event == null)
      throw "Mouse event can't be null!";
    return this.worldPositionToIndex(this.pointerToWorldPosition(event));
  }

  private worldPositionToTile(worldPos: Vector2): MapTile {

    if (worldPos == null)
      throw "Position can't be null!";

    const index = new Vector2(Math.round(worldPos.x), Math.round(worldPos.y));

    return this.displayedTerritories.getDisplayedTile(index);
  }

  private worldPositionToIndex(worldPos: Vector2): Vector2 {
    if (worldPos == null)
      throw "Position can't be null!";

    return new Vector2(Math.round(worldPos.x), Math.round(worldPos.y));
  }

  private pointerToWorldPosition(event: PointerEvent): Vector2 {
    const screenPos = new Vector2(
      (event.layerX / this.renderTarget.getWidth()) * 2 - 1,
      - (event.layerY / this.renderTarget.getHeight()) * 2 + 1
    );

    let direction = new Vector3();
    this.camera.getWorldDirection(direction);

    this.raycaster.setFromCamera(screenPos, this.camera);
    this.raycaster.ray.origin.subVectors(this.raycaster.ray.origin, direction.multiplyScalar(100));

    const plane = new Mesh(new PlaneGeometry(10000, 10000));

    const intersects = this.raycaster.intersectObjects([plane], false);
    if (intersects.length > 0) {
      return new Vector2(intersects[0].point.x, intersects[0].point.y);
    }

    console.error('World position not found! Returning Vector2.zero.')

    return new Vector2();
  }

  private moveCameraTowards(position: Vector3, deltaTime: number, overrideTarget = false) {
    const dist = this.camera.position.distanceTo(position);
    const moveBy = this.CAMERA_SPEED * deltaTime + this.CAMERA_SPEED_MODIFIER * dist * deltaTime;
    if (moveBy >= dist) {
      this.setCameraPosition(position, overrideTarget);
    } else {
      const direction = new Vector3().subVectors(position, this.camera.position);
      direction.divideScalar(dist);
      this.setCameraPosition(this.camera.position.clone().addScaledVector(direction, moveBy), overrideTarget);
    }
  }

  axis = '';

  setZoom(amount: number) {
    const sizeFrac = 1 - amount;
    this.cameraSize = this.MIN_CAMERA_SIZE + (this.MAX_CAMERA_SIZE - this.MIN_CAMERA_SIZE) * sizeFrac;
    this.zoomSlider.nativeElement.value = amount * 100; 
    this.renderTargetResized();
  }

  renderTargetResized() {
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.setCameraSize(this.calculateCameraSize(this.cameraSize, this.renderTarget.aspectRatio()));

    this.camera.updateProjectionMatrix();

    this.renderer.setSize(this.renderTarget.getWidth(), this.renderTarget.getHeight());
  }

  private calculateCameraSize(orthographicSize, aspectRatio): OrthCameraSize {
    return {
      left: -aspectRatio * orthographicSize,
      right: aspectRatio * orthographicSize,
      top: orthographicSize,
      bottom: -orthographicSize,
      near: -100,
      far: 100
    };
  }

  private setCameraSize(size: OrthCameraSize) {
    this.camera.left = size.left;
    this.camera.right = size.right;
    this.camera.top = size.top;
    this.camera.bottom = size.bottom;
    this.camera.near = size.near;
    this.camera.far = size.far;
  }



  private update() {
    const deltaTime = this.clock.getDelta();

    this.onUpdate.next(deltaTime);

    this.moveCameraTowards(this.targetCameraPosition, deltaTime, false);

    if (this.currentCameraSpeed.lengthSq() != 0 && !this.pointerHeldDown) {
      const shift = new Vector3().copy(this.currentCameraSpeed).multiplyScalar(deltaTime);
      this.setCameraPosition(new Vector3().copy(this.camera.position).add(shift), true);

      const decay = this.CAMERA_SPEED_DAMP * deltaTime;
      if (this.currentCameraSpeed.lengthSq() < decay * decay) {
        this.currentCameraSpeed.set(0, 0, 0);
      } else {
        this.currentCameraSpeed.sub(new Vector3().copy(this.currentCameraSpeed).multiplyScalar(decay));
      }
      this.ensureCameraTargetInsideMap(true);
    }

    this.updateMapSidesCoverPosition();
    this.updateFlashlightPosition();
  }

  /**
   * Updates the position of the map sides cover.
   * Should be called after the camera has moved.
   */
  private updateMapSidesCoverPosition() {
    const coverPos = this.camera.position.clone();
    //coverPos.z = 1;
    //coverPos.x -= 1;
    //coverPos.y -= 1;

    this.mapSidesCover.position.copy(coverPos);
  }

  /**
   * Updates the position of the flash light.
   * Should be called every time the camera has moved.
   */
  private updateFlashlightPosition() {
    const pos = this.camera.position.clone();
    pos.z = this.flashLight.position.z;

    this.flashLight.position.copy(pos);
  }

  /**
   * @returns An observable, which is emits a call on every update frame.
   */
  getUpdateObservable(): Observable<number> {
    return this.onUpdate.asObservable();
  }

  ngOnDestroy() {
    this.interactionsEnabledSub.unsubscribe();
  }

  /**
   * Renders the scene onto the canvas.
   */
  private render() {
    this.renderer.render(this.mainScene, this.camera);
  }

  // //For testing
  // private angleAdjust() {
  //   window.onkeyup = (event) => {
  //     this.axis = event.key;
  //     let amount = 0;
  //     if (event.key == '+') {
  //       amount = 3;
  //     }
  //     if (event.key == '-') {
  //       amount = -3;
  //     }
  //     if (this.axis == 'x')
  //       this.camera.rotation.x += amount * Math.PI / 180;
  //     if (this.axis == 'y')
  //       this.camera.rotation.y += amount * Math.PI / 180;
  //     if (this.axis == 'z')
  //       this.camera.rotation.z += amount * Math.PI / 180;

  //     console.log(this.camera.rotation.x * 180 / Math.PI, this.camera.rotation.y * 180 / Math.PI, this.camera.rotation.z * 180 / Math.PI);
  //   };

  //   window.onpointermove = (event) => {
  //     if (this.axis == 'x')
  //       this.camera.rotation.x += event.movementX * Math.PI / 180;
  //     if (this.axis == 'y')
  //       this.camera.rotation.y += event.movementX * Math.PI / 180;
  //     if (this.axis == 'z')
  //       this.camera.rotation.z += event.movementX * Math.PI / 180;
  //     console.log(this.camera.rotation.x * 180 / Math.PI, this.camera.rotation.y * 180 / Math.PI, this.camera.rotation.z * 180 / Math.PI);
  //   };
  // }
}
