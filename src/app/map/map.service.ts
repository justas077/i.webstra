import { MapTerritory, IncomingTroops } from './map-territory';
import { Subject, Observable } from 'rxjs';
import { Vector2 } from 'three';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../auth/auth-service';
import { MapComponent } from './map.component';
import { environment } from 'src/environments/environment';
import { ConfigurationService, MapConfiguration } from '../game/configuration-service';
import { TimeService, TIME_DELAY } from '../game/time.service';
import { User } from '../auth/user';
import  * as moment from 'moment';
import { PlayerService } from '../auth/player.service';
import { deepCopy } from '../shared/utilities';

/**
 * Used to manage the state of the map
 */
@Injectable()
export class MapService 
{
    //===Events=====================
    private newChunksLoaded: Subject<void> = new Subject(); //Called when new chunks of territories are loaded
    onTileHoverChanged: Subject<MapTerritory> = new Subject();
    onSelectedTileChanged: Subject<MapTerritory> = new Subject();
    onTileClicked: Subject<MapTerritory> = new Subject();
    interactionsEnabledChanged: Subject<boolean> = new Subject();
    onTerritoryUpdated: Subject<MapTerritory> = new Subject();
    //===Subscribtions==============
    private mapSize: Vector2 = new Vector2(100, 100); //The size of the map in territories

    private chunkSize: number = 10; //The width and height of the map chunk in territories
    private mapSizeChunks: Vector2;

    private LOAD_AROUND_FOCUSED_DISTANCE = 30; //How many territories around the focused territory should be preloaded

    private matrix: MapTerritory[][];
    private loadedChunks: boolean[][]; //Which of the chunks were loaded

    private userTerritories: MapTerritory[] = [];

    private interactionsEnabled: boolean = true; //Are user interactions with the map enabled

    private apiUrl: string = '';

    //Is all the information for showing the map for the user loaded
    private initialStateLoaded: boolean = false; 

    

    //When was the last time the troops were updated
    private lastTroopProduceUpdate: moment.Moment = null;

    private troopUpdateTimer;

    private mapComponent: MapComponent;

    constructor(
        private http: HttpClient, 
        private authService: AuthService, 
        private configurationService: ConfigurationService,
        private time: TimeService,
        private playerService: PlayerService
    ) {
        this.apiUrl = environment.apiUrl;

        this.configurationService.mapConfigurationChanged.subscribe(
            (result: MapConfiguration) => {
                this.mapSize.copy(result.size);
                this.chunkSize = result.chunkSize;

                this.mapSizeChunks = new Vector2(
                    Math.ceil(this.mapSize.x / this.chunkSize), 
                    Math.ceil(this.mapSize.y / this.chunkSize));

                this.matrix = [];
                for(let i = 0; i < this.mapSize.x; i++) {
                    this.matrix.push([]);
                    for(let j = 0; j < this.mapSize.y; j++) {
                        this.matrix[i].push(null);
                    }
                }
            
                this.loadedChunks = [];
                for(let i = 0; i < this.mapSizeChunks.x; i++) {
                    this.loadedChunks[i] = [];
                    for(let j = 0; j < this.mapSizeChunks.y; j++) {
                        this.loadedChunks[i].push(false);
                    }
                }
            }
        );
    }

    /**
     * Gets the index of the tile that should focused opon openning the map.
     */
    getDefaultTargetIndex() : Vector2 {
        if(this.userTerritories.length == 0)
            return new Vector2(0, 0);
        return this.userTerritories[0].index.clone();
    }

    /**
     * @returns A shallow copy of the player's territories.
     */
    getUserTerritories() : MapTerritory[] {
        return this.userTerritories.slice();
    }

    /**
     * Loads the information of the map that is needed to start the game.
     */
    loadInitialState() : Observable<void> {
        this.http.get(this.apiUrl + 'map/current-user').subscribe(
            (result: []) => {
                console.log(result);
                result.forEach((ter: any) => {
                    const territory: MapTerritory = new MapTerritory();
                    territory.id = ter['id'];
                    territory.index = new Vector2(ter['x'], ter['y']);
                    territory.owner = new User();
                    territory.owner.id = ter['owner']['id'];
                    territory.owner.username = ter['owner']['username'];
                    territory.owner.isCurrentPlayer = true;
   
                    territory.landscapeType = ter['type'];
                    territory.villageType = ter['village_type'];
                    territory.villageLevel = ter['village_level'];
                    territory.producesTroop = ter['produce_troop'];
                    territory.troopHr = ter['troops_per_hour'];
                    territory.goldHr = ter['gold_per_hour'];

                    territory.troops = ter['troops'];
                    if(ter['incoming_troops'] != null) {
                        for(let entry of ter['incoming_troops']) {
                            const incTroops = new IncomingTroops();
                            incTroops.willArriveAt = this.time.dateFromBackend(entry['will_arrive_at']);
                            incTroops.from = new Vector2(entry['from']['x'], entry['from']['y']);
                            incTroops.troops = entry['troops'];
                            territory.incomingTroops.push(incTroops);
                        }
                    }

                    this.matrix[territory.index.x][territory.index.y] = territory;
                    this.userTerritories.push(territory);
                })

                this.lastTroopProduceUpdate = this.time.nowMoment;
                this.troopUpdateTimer = setTimeout(() => this.updateTroopProduction(), this.timeToTroopProduction());
                
                //console.log(result)
                //console.log(this.userTerritories);
                this.initialStateLoaded = true;
            },
            (error) => {
                console.log(error);
            }
        );
        
        return new Observable();
    }

    private updateTroopProduction() {
        const curMoment = this.time.nowMoment;
        const hoursElapsed = curMoment.diff(this.lastTroopProduceUpdate) / 3600000;
        //console.log(this.lastTroopProduceUpdate, curMoment);
        this.userTerritories.forEach(territory => {
            if(territory.producesTroop != null && territory.troopHr != null) {
                const prevTroops = Math.trunc(territory.troops[territory.producesTroop]);
                territory.troops[territory.producesTroop] += hoursElapsed * territory.troopHr;

                if (prevTroops != Math.trunc(territory.troops[territory.producesTroop])) {
                    this.playerService.incrementTroop(territory.producesTroop);
                    this.onTerritoryUpdated.next(territory);
                }
            }
        });

        this.lastTroopProduceUpdate = curMoment;

        setTimeout(() => this.updateTroopProduction(), this.timeToTroopProduction());
    }

    getTroopProductionProgress(ter: MapTerritory): number {
        if (ter.producesTroop == null || ter.troopHr == null || ter.troopHr == 0)
            return 0;
        if (ter.troops[ter.producesTroop] == null)
            return 0;

        let frac = ter.troops[ter.producesTroop] - Math.trunc(ter.troops[ter.producesTroop]);
        frac += this.time.hoursFrom(this.lastTroopProduceUpdate) * ter.troopHr;
        return Math.max(0, Math.min(1, frac));
    }

    /**
     * @returns Miliseconds, after which the troops should be updated again.
     */
    timeToTroopProduction(): number {
        let minMs: number = 60000;
        this.userTerritories.forEach( t => {
            if(t.producesTroop != null && t.troopHr != null && t.troopHr > 0) {
                const leftOfTroop = 1 - t.troops[t.producesTroop] + Math.trunc(t.troops[t.producesTroop]);
                const msPerTroop = 3600000 / t.troopHr;
                const msleft = leftOfTroop * msPerTroop;

                minMs = Math.min(minMs, msleft);
            }
        });
        return Math.max(1000, minMs) + 200;
    }

    getMapComponent() {
        return this.mapComponent;
    }

    /**
     * @returns An observable, which is invoked every time new map chunks are loaded.
     */
    getNewChunksLoadedObservable() : Observable<void> {
        return this.newChunksLoaded.asObservable();
    }

    /**
     * Gets rectangle of territories.
     * @param fromIndex The min index of the territory (inclusive).
     * @param toIndex The max index of the territory (exclusive).
     * @returns A 2D array containing the requested territories.
     */
    getFromToTerritories(fromIndex: Vector2, toIndex: Vector2) : MapTerritory[][] {
        if(fromIndex.x >= toIndex.x || fromIndex.y >= toIndex.y)
            throw 'fromIndex must be less than toIndex!';

        const result = [];
        for (let i = fromIndex.x; i < toIndex.x; i++) {
            result.push([]);
            for (let j = fromIndex.x; j < toIndex.y; j++) {
                result.push(this.matrix[i][j]);
            }
        }
        return result;
    }

    /**
     * @param index The index of the territory.
     * @returns The territory at the index or null if the territory isn't loaded
     * or the index is out of bounds.
     */
    getTerritory(index: Vector2) {

        if (index.x < 0 || index.y < 0 || index.x >= this.mapSize.x || index.y >= this.mapSize.y)
            return null;
        return this.matrix[index.x][index.y];
    }

    /**
     * @returns A clone of the size of the map.
     */
    getMapSize() : Vector2 {
        return this.mapSize.clone();
    }

    /**
     * @param index The index to check.
     * @returns Whether the given index is in the boundaries of the map
     */
    isIndexInsideMap(index: Vector2) : boolean {
        return !(index.x < 0 || index.y < 0 || index.x >= this.mapSize.x || index.y >= this.mapSize.y);
    }

    setMapCompoenent(mapComponent: MapComponent) {
        this.mapComponent = mapComponent;
    }

    addIncomingTroopsFor(forIndex: Vector2, moveResponse) {
        const incTroops = new IncomingTroops();
        incTroops.willArriveAt = this.time.dateFromBackend(moveResponse['will_arrive_at']);
        incTroops.from = new Vector2(moveResponse['from']['x'], moveResponse['from']['y']);
        incTroops.troops = moveResponse['troops'];
        this.matrix[forIndex.x][forIndex.y].incomingTroops.push(incTroops);

        this.onTerritoryUpdated.next(this.matrix[forIndex.x][forIndex.y]);
    }

    removeTroopsFrom(territory: MapTerritory, troopsToRemove) {
        for(let troop in troopsToRemove) {
            territory.troops[troop] -= troopsToRemove[troop];
        }

        this.onTerritoryUpdated.next(territory);
    }

    evaluateIncomingTroopsFor(territory: MapTerritory): boolean {
        let changed: boolean = false;
        if(territory.incomingTroops.length == 0)
            false;
        for(let i = 0; i < territory.incomingTroops.length; i++) {
            if(territory.incomingTroops[i].willArriveAt <= new Date()) {
                changed = true;
                for(let troop in territory.incomingTroops[i].troops) {
                    territory.troops[troop] += territory.incomingTroops[i].troops[troop];
                }
                territory.incomingTroops.splice(i, 1);
                i--;
            }
        }
        if(changed)
            this.onTerritoryUpdated.next(territory);
        return changed;
    }

    loadTerritory(position: Vector2) {

    }

    /**
     * Tries to load chunks from the server.
     * @param chunks The chunks to load.
     * @returns An observable, which either completes, 
     * if all the chunks were successfully loaded,
     * or fails if there were errors.
     */
    private loadChunks(chunks: Vector2[]){
        const body = { chunks: chunks };

        const headers = new HttpHeaders({'Accept' : 'application/json'});

        this.http.put(this.apiUrl + "map/chunks", body)
        .subscribe(
            (result: any[]) => {
                //console.log(result);
                result.forEach(element => {
                    if(this.matrix[element['x']][element['y']] == null) {
                        this.matrix[element['x']][element['y']] = MapTerritory.createFromBackend(element);
                    }
                });
                this.newChunksLoaded.next();
            },
            (error) => {
                //Set the chunks as not loaded and try again and what not
                console.log(error);
            }
        );
    }

    /**
     * Called when the focused territory changes.
     * @param index The index of the new focused territory.
     * Null if no territory is focused anymore.
     */
    focusedTerritoryChanged(index: Vector2) {
        const chunksToLoad: Vector2[] = []

        const from = new Vector2(
            Math.max(index.x - this.LOAD_AROUND_FOCUSED_DISTANCE, 0),
            Math.max(index.y - this.LOAD_AROUND_FOCUSED_DISTANCE, 0));  
        const to = new Vector2(
            Math.min(index.x + this.LOAD_AROUND_FOCUSED_DISTANCE, this.mapSize.x-1),
            Math.min(index.y + this.LOAD_AROUND_FOCUSED_DISTANCE, this.mapSize.x-1));  
        
        const fromChunk = new Vector2(Math.trunc(from.x / this.chunkSize), Math.trunc(from.y / this.chunkSize)) ;
        const toChunk = new Vector2(Math.trunc(to.x / this.chunkSize), Math.trunc(to.y / this.chunkSize));

        for(let i = fromChunk.x; i <= toChunk.x; i++) 
        {
            for(let j = fromChunk.y; j <= toChunk.y; j++) 
            {
                if(this.loadedChunks[i][j] == false) {
                    chunksToLoad.push(new Vector2(i, j));
                    this.loadedChunks[i][j] = true;
                }
            }        
        }

        if (chunksToLoad.length > 0)
            this.loadChunks(chunksToLoad);
    }

    /**
     * Enables or disables the interactions with the map.
     * @param enabled Should the interactions be enabled.
    */
    setInteractionsEnabled(enabled: boolean) {
        this.interactionsEnabled = enabled;
        this.interactionsEnabledChanged.next(enabled);
    }
    /**
     * @returns True, if interactions are enbaled, false otherwise.
     */
    areInteractionsEnabled() : boolean {
        return this.interactionsEnabled;
    }

    /**
     * Checks if all the required information for showing the map to the user loaded.
     * @returns True, if it's loaded, false otherwise.
     */
    isInitialStateLoaded() : boolean {
        return this.initialStateLoaded;
    }
}