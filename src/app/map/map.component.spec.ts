import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapComponent } from './map.component';
import { MapService } from './map.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { AuthService } from '../auth/auth-service';

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;
  let service: MapService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapComponent ],
      providers: [ MapService, HttpClient, HttpHandler, AuthService ]
    })
    .compileComponents();
    
    
  }));

  beforeEach(() => {
    TestBed.configureTestingModule
    
    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
