import { Vector2, Color } from 'three';
import { User } from '../auth/user';
import { GameConfiguration } from '../game/configuration-service';
import { MapService } from './map.service';
import { ResourceService } from '../game/resource-service';
import { TimeService } from '../game/time.service';

export const NATURE_ID = 2;
export class MapTerritory {
    id: number;
    index: Vector2 = new Vector2();
    owner: User;
    
    landscapeType: string = "Not set";
    villageType: string;
    villageLevel: number;

    producesTroop: string;
    troopHr: number;
    goldHr: number;
    
    troops: {[type: string]: number };
    incomingTroops: IncomingTroops[] = [];

    /**
     * @returns An array that contains the troops of the territory, each element
     * defines the name of the troop, the type and the amount of them. Empty array if no troops are
     * in the territory.
     * @param config The game configuration.
     */
    getTroopsTypeNameAmount(config: GameConfiguration)
        :{ name: string, type: string, amount: number }[] 
    {
        if(this.troops == null)
            return null;
        const dict = this.owner.id == NATURE_ID ? config.natureTroops : config.playerTroops;
        const result = [];
        for(let type in this.troops) {
            if (Math.trunc(this.troops[type]) != 0) {
                result.push({
                    name: dict[type] == null ? "ERROR" : dict[type].name,
                    amount: Math.trunc(this.troops[type]),
                    type: type
                });
            }
        }
        //console.log(result);
        return result;
    }

    getIncomingTroopArray(config: GameConfiguration, timeService: TimeService): { name: string, timeLeft: number, amount: number } [] {
        if(this.incomingTroops.length == 0)
            return null;

        const dict = this.owner.id == NATURE_ID ? config.natureTroops : config.playerTroops;
        const result: { name: string, timeLeft: number, amount: number }[] = [];
        for(let inc of this.incomingTroops) {
            const sLeft = Math.max(0, timeService.diffSeconds(inc.willArriveAt, new Date()));
            for(let troop in inc.troops) {
                if(inc.troops[troop] > 0) {
                    result.push({name: dict[troop].name, amount: inc.troops[troop], timeLeft: sLeft });
                }
            }
        }
        return result;
    }

    isOwnedByNature(): boolean {
        return this.owner == null || this.owner.id == NATURE_ID;
    }

    getOwnerNameWhose() {
        if (this.owner == null || this.owner.username == null) {
            return 'Unknown';
        }
        if(this.owner.id == NATURE_ID)
            return 'Nature';
        return this.owner.username + "'s";
    }

    getOwnedByCurrentPlayer() {
        if (this.owner == null || this.owner.username == null) {
            return false;
        }
        return this.owner.isCurrentPlayer;
    }

    getProducedTroopName(config: GameConfiguration) : string {
        if (this.producesTroop == null)
            return null;
        if(this.producesTroop.length == 0)
            return 'none';
        if (this.owner.id == NATURE_ID) {
            return config.natureTroops[this.producesTroop].name;
        } 
        return config.playerTroops[this.producesTroop].name;
    }

    getTroopProductionText(config: GameConfiguration): string {
        const producedTroopName = this.getProducedTroopName(config);
        if(producedTroopName == null || this.troopHr == null || this.troopHr == 0)
            return null;
        
        return producedTroopName + ' - ' + this.troopHr + '/hour';
    }

    getLandscapeTextColor(resources: ResourceService) : string {
        return resources.getTerritoryLandscapeColor(this.landscapeType);
    }

    getFormattedCoordinates(mapService: MapService) : string {
        const start = mapService.getMapSize();
        start.set(Math.trunc(start.x / 2), Math.trunc(start.y / 2));
        return ((this.index.x - start.x) + ' : ' + (this.index.y - start.y));
    }

    static createFromBackend(retrieved) : MapTerritory{
        const result = new MapTerritory();
        result.id = retrieved['id'];
        result.index = new Vector2(retrieved['x'], retrieved['y']);
        result.owner = { id: retrieved['owner']['id'], username: retrieved['owner']['username'], isCurrentPlayer: false };
        result.landscapeType = retrieved['type'];
        result.villageType =  null;
        result.villageLevel = null;
        result.producesTroop = null;
        result.troopHr = null;
        result.goldHr = null;
        result.troops = null; 
        return result;
    }
}

export class IncomingTroops {
    relocationType: string;
    from: Vector2;
    willArriveAt: Date;
    troops: {[type: string]: number };
}
