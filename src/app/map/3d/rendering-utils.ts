import { MapTerritory } from '../map-territory';

export class OrthCameraSize {
    left: number;
    right: number;
    top: number;
    bottom: number;
    near: number;
    far: number;
}

export function randomColor() {
    return ['darkolivegreen', 'aqua', 'coral', 'green'][Math.floor(Math.random() * 4)];
}

export class HoverState {
    type: HoverType;
    hoveredTerritory: MapTerritory;
}

export enum TileType {
    Disabled,
    Loading,
    Regular
}

export enum HoverType {
    Nothing,
    Territory,
    LoadingTile
}

export function moveTowards(from: number, to: number, maxDelta: number) : number {
    const diff = to - from;
    if(Math.abs(diff) <= maxDelta)
        return to;
    return from + Math.sign(diff) * maxDelta;
}