import { MapTile } from './map-tile';
import { Vector2, Scene } from 'three';
import { MapService } from '../map.service';
import { MapComponent } from '../map.component';
import { ResourceService } from 'src/app/game/resource-service';
import { MapTerritory } from '../map-territory';

/**
 * Defines a part of map that is displayed on the screen
 */
export class DisplayedTerritories 
{
    private RAISED_HEIGHT: number = 0.25;

    public size: number = 11;
    private tiles: MapTile[][];
    //Defines how much do the indexes in the local array differ from the global one in the map service
    private areaPosition: Vector2 = new Vector2(0, 0); 

    private initialized: boolean = false;

    private raisedIndexes: Vector2[] = [];

    private selectedTile: MapTile;
    private hoveredTile: MapTile;
    
    private selectedIndex: Vector2;
    private hoveredIndex: Vector2;
    
    private raisedTiles: MapTile[] = [];

    constructor(
        private mapComponenet: MapComponent, 
        private mainScene: Scene, 
        private mapService: MapService, 
        private resourceService: ResourceService
    ){
        this.tiles = [];
        for (let i = 0; i < this.size; i++) {
            this.tiles.push([]);
            for (let j = 0; j < this.size; j++) {
                this.tiles[i].push(null);
            }
        }
    }

    /**
     * Creates the tile objects.
     */
    initTiles() {
        for (let i = 0; i < this.tiles.length; i++) {
            for (let j = 0; j < this.tiles[i].length; j++) {
                this.tiles[i][j] = new MapTile(this.resourceService, this.mainScene);
                this.tiles[i][j].setPosition(i + this.areaPosition.x, j + this.areaPosition.y, 0);
            }
        }

        this.mapComponenet.getUpdateObservable().subscribe((deltaTime: number) => {
            this.update(deltaTime);
        })

        this.mapService.getNewChunksLoadedObservable().subscribe(() => {
            this.onNewChunksLoaded();
        });

        this.mapService.onTileHoverChanged.subscribe( 
            (t) => this.hoveredIndexChanged(t == null ? null : t.index) );
        this.mapService.onSelectedTileChanged.subscribe(
            (t) => this.selectedIndexChanged(t == null ? null : t.index) );

        this.initialized = true;
    }

    private hoveredIndexChanged(index: Vector2) {
        this.hoveredIndex = index;
        if(this.hoveredTile != null) {
            this.hoveredTile.setHovered(false);
        }
        
        this.hoveredTile = this.hoveredIndex == null ? null : this.getDisplayedTile(this.hoveredIndex);

        if(this.hoveredTile != null)
            this.hoveredTile.setHovered(true);
    }

    private selectedIndexChanged(index: Vector2) {
        this.selectedIndex = index;
        if (this.selectedTile != null) {
            this.selectedTile.setSelected(false);
        }

        this.selectedTile = this.selectedIndex == null ? null : this.getDisplayedTile(this.selectedIndex);

        if(this.selectedTile != null)
            this.selectedTile.setSelected(true);

        this.setRaisedIndexes(this.selectedIndex == null ? [] : [this.selectedIndex]);
    }

    setRaisedIndexes(indexes: Vector2[]) {
        this.raisedIndexes = indexes;
        this.raisedTiles.forEach(t => t.setTargetZ(0));
        this.raisedTiles = [];

        this.raisedIndexes.forEach(i => {
            const tile = this.getDisplayedTile(i);
            if(tile != null) {
                tile.setTargetZ(this.RAISED_HEIGHT);
                this.raisedTiles.push(tile);
            }
        });
    }

    /**
     * Called every time new chunks are loaded to the map service
     */
    onNewChunksLoaded() {
        for (let i = 0; i < this.size; i++) {
            for (let j = 0; j < this.size; j++) {
                if (!this.tiles[i][j].hasTerritoryLoaded()) {
                    const territory = this.mapService.getTerritory(new Vector2(i + this.areaPosition.x, j + this.areaPosition.y));
                    if (territory != null) {
                        this.tiles[i][j].applyTerritory(territory);
                    }
                }
            }
        }
    }

    /**
     * Finds and returns the displayed tile.
     * @param globalIndex The global index of the tile.
     * @returns The tile at the given index or null if such
     * an index doesn't exist or the tile at the given index isn't displayed
     */
    getDisplayedTile(globalIndex : Vector2) : MapTile {
        if(globalIndex == null)
            return null;
        const localIndex = new Vector2(globalIndex.x - this.areaPosition.x, globalIndex.y - this.areaPosition.y);
        if(localIndex.x < 0 || localIndex.y < 0 || localIndex.x >= this.size || localIndex.y >= this.size)
            return null;

        if(this.tiles[localIndex.x][localIndex.y] == null || this.tiles[localIndex.x][localIndex.y].isDisabled())
            return null;
        return this.tiles[localIndex.x][localIndex.y];
    }

    /**
     * Finds and returns the displayed territory.
     * @param globalIndex The global index of the tile.
     * @returns The displayed territory at the given index.
     * Null if tile at the index isn't displayed, such tile doesn't exist or 
     * such tile is displayed but it doesn't have a loaded territory.
     */
    getDisplayedTerritory(globalIndex : Vector2) : MapTerritory {
        const tile: MapTile = this.getDisplayedTile(globalIndex);
        return tile == null ? null : tile.getTerritory();
    }

    /**
     * Changes the position of the displayed area.
     * @param position The index of the bottom-left corner of the area that should be displayed.
     */
    setAreaPosition(position: Vector2) {
        if(!this.initialized) {
            this.areaPosition = position;
            return;
        }

        this.shiftPositions(position.clone().sub(this.areaPosition));
        this.areaPosition = position.clone();

        //After shifting, change the target territories where needed
        for (let i = 0; i < this.size; i++) {
            for (let j = 0; j < this.size; j++) {
                const globalIndex = new Vector2(i + this.areaPosition.x, j + this.areaPosition.y);
                const inMap = this.mapService.isIndexInsideMap(globalIndex);
                if(!inMap) {
                    this.tiles[i][j].setDisabled(true);
                } else {
                    this.tiles[i][j].setDisabled(false);

                    const territory = this.mapService.getTerritory(new Vector2(i + this.areaPosition.x, j + this.areaPosition.y));

                    if (this.tiles[i][j].getTerritory() != territory) {
                        this.tiles[i][j].applyTerritory(territory);
                    }
                }
            }
        }
        //After shifting, select/hover tiles if needed
        
        if(this.hoveredIndex != null && this.hoveredTile == null)
            this.hoveredIndexChanged(this.hoveredIndex);
            
        if (this.selectedIndex != null && this.selectedTile == null) {
            this.selectedTile = this.getDisplayedTile(this.selectedIndex);

            if (this.selectedTile != null){
                this.selectedTile.setSelected(true);
                this.setRaisedIndexes(this.selectedIndex == null ? [] : [this.selectedIndex]);
            }   
        }
    }

    /**
     * Changes the position of the displayed area, so that the given position
     * is at the center of the displayed map.
     * @param position The index of center of the area that should be displayed.
     */
    setAreaCenterPosition(position: Vector2) {
        this.setAreaPosition(new Vector2(
            Math.trunc(position.x - this.size * 0.5), 
            Math.trunc(position.y - this.size * 0.5)));
    }

    /**
     * Shifts the displayed area in relation to the overall array.
     * @param shift The distance to shift.
     */
    private shiftPositions(shift: Vector2) {
        if (Math.abs(shift.x) >= this.size || Math.abs(shift.y) >= this.size) {
            //If the shift is bigger than the map size, there's no point in shifting, just change all positions
            for (let i = 0; i < this.size; i++) {
                for (let j = 0; j < this.size; j++) {
                    this.tiles[i][j].translate(shift.x, shift.y);
                }
            }
        } else {
            //Otherwise, change the positions only of those tiles that appear/dissapear
            if (shift.x > 0)
                this.shiftPositionsRight(shift.x);
            else if (shift.x < 0)
                this.shiftPositionsLeft(Math.abs(shift.x));

            if (shift.y > 0)
                this.shiftPositionsUp(shift.y);
            else if (shift.y < 0)
                this.shiftPositionsDown(Math.abs(shift.y));
        }
    }

    /**
     * Shifts left the area in relation to the overall array.
     * Those tiles that get out of bounds are added to the other side.
     * @param shift The distance to shift.
     */
    private shiftPositionsLeft(shift: number) {
        if (shift > this.size)
            throw 'CANNOT SHIFT MORE THAN THE WIDTH OF THE MAP!';
        for (let i = 0; i < shift; i++) {
            for (let j = 0; j < this.size; j++) {
                this.tiles[this.size - i - 1][j].translate(-this.size, 0);
            }
        }
        const tempArray = this.tiles.slice();
        for (let i = 0; i < this.size; i++) {
            this.tiles[(i + shift) % this.size] = tempArray[i];
        }
        
    }

    /**
     * Shifts right the area in relation to the overall array.
     * Those tiles that get out of bounds are added to the other side.
     * @param shift The distance to shift.
     */
    private shiftPositionsRight(shift: number) {
        if (shift > this.size)
            throw 'CANNOT SHIFT MORE THAN THE WIDTH OF THE MAP!';

        for (let i = 0; i < shift; i++) {
            for (let j = 0; j < this.size; j++) {
                this.tiles[i][j].translate(this.size, 0);
            }
        }
        const tempArray = this.tiles.slice();
        for (let i = 0; i < this.size; i++) {
            this.tiles[i] = tempArray[(i + shift) % this.size];
        }
    }

    /**
     * Shifts up the area in relation to the overall array.
     * Those tiles that get out of bounds are added to the other side.
     * @param shift The distance to shift.
     */
    private shiftPositionsUp(shift: number) {
        if (shift > this.size)
            throw 'CANNOT SHIFT MORE THAN THE WIDTH OF THE MAP!';

        for (let i = 0; i < this.size; i++) {
            for (let j = 0; j < shift; j++) {
                this.tiles[i][j].translate(0, this.size);
            }
        }
        const tempArray = this.tiles.slice().map(function (row) { return row.slice(); });
        for (let i = 0; i < this.size; i++) {
            for (let j = 0; j < this.size; j++) {
                this.tiles[i][j] = tempArray[i][(j + shift) % this.size];
            }
        }
    }

    /**
     * Shifts down the area in relation to the overall array.
     * Those tiles that get out of bounds are added to the other side.
     * @param shift The distance to shift.
     */
    private shiftPositionsDown(shift: number) {
        if (shift > this.size)
            throw 'CANNOT SHIFT MORE THAN THE WIDTH OF THE MAP!';

        for (let i = 0; i < this.size; i++) {
            for (let j = 0; j < shift; j++) {
                this.tiles[i][this.size - j - 1].translate(0, -this.size);
            }
        }
        const tempArray = this.tiles.slice().map(function (row) { return row.slice(); });
        for (let i = 0; i < this.size; i++) {
            for (let j = 0; j < this.size; j++) {
                this.tiles[i][(j + shift) % this.size] = tempArray[i][j];
            }
        }
    }

    /**
     * Executed on every frame, before the frame is rendered.
     * @param deltaTime Time since last update.
     */
    update(deltaTime: number) {
        for(let i = 0; i < this.size; i++) {
            for (let j = 0; j < this.size; j++) {
                if(this.tiles[i][j] != null) {
                    this.tiles[i][j].update(deltaTime);

                    // const dist = Math.max(
                    //     Math.abs(i - (this.size * 0.5)),
                    //     Math.abs(j - (this.size * 0.5))
                    //     );

                    // this.tiles[i][j].copyPosition(this.tiles[i][j].getPosition().setZ(((this.size - dist*4) * 1 / this.size)));
                }
            }
        }
    }

    /**
     * @returns The size(width and height) in territories of the displayed map fraction.
     */
    getSize() : number {
        return this.size;
    }

    // addTestTiles() {
    //     for (let i = 0; i < this.tiles.length; i++) {
    //         for (let j = 0; j < this.tiles[i].length; j++) {
    //             this.tiles[i][j] = new MapTile(this.mapResources, null);
    //             this.tiles[i][j].setPosition(i + this.tilesCoordinatesOffset.x, j + this.tilesCoordinatesOffset.y, 0);
    //             this.mainScene.add(this.tiles[i][j].container);
    //         }
    //     }
    // }
}