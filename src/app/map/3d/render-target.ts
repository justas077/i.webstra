export class RenderTarget {
    
    constructor(private element) {

    }

    getElement() {
        return this.element;
    }

    aspectRatio() {
        return this.element.clientWidth / this.element.clientHeight;
    }

    getWidth() {
        return this.element.getBoundingClientRect().width;
    }

    getHeight() {
        return this.element.getBoundingClientRect().height;
    }
}