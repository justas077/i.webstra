import { Mesh, Object3D, AnimationMixer, Vector3, Scene } from 'three';
import { MapTerritory } from '../map-territory';
import { ResourceService } from 'src/app/game/resource-service';
import { moveTowards } from './rendering-utils';

export class MapTile {
    private container: Object3D;
    private base: Mesh;
    private animationMixer: AnimationMixer;

    private disabled: boolean = true;

    private targetTerritory: MapTerritory = null;

    private hovered: boolean = false;
    private selected: boolean = false;

    private border: Mesh;

    private targetZ: number = 0;

    private topPlate: Mesh;

    private model: Mesh;


    constructor(private resourceService: ResourceService, private mainScene: Scene) {
        this.container = new Object3D();

        this.base = resourceService.newBaseMesh();
        this.base.position.setZ(-0.5);

        this.topPlate = resourceService.newTerritoryPlate();
        this.topPlate.position.setZ(0.0001);

        this.border = resourceService.newBorder();
        this.border.position.setZ(0.0002);

        this.container.add(this.base);
        this.container.add(this.border);
        this.container.add(this.topPlate);
        
        mainScene.add(this.container);

        // this.animationMixer = new AnimationMixer(this.house.children[0]);
        // const action = this.animationMixer.clipAction(resourceService.getHouseRotateAnimation());
        // action.play();
    }

    /**
     * Sets the target territory for the tile. Only works if the tile is enabled.
     * @param targetTerritory The territory which should be represented by this tile. Null if the territory is loading.
     */
    applyTerritory(targetTerritory: MapTerritory) {
        //console.log(targetTerritory, this.targetTerritory);
        if(this.disabled && targetTerritory != null)
            return;
        if(targetTerritory == this.targetTerritory)
            return;

        if (this.model != null) {
            this.container.remove(this.model);
            this.model.geometry.dispose();
            this.model = null;
        }

        this.targetTerritory = targetTerritory;

        if(targetTerritory == null) {
            this.setHovered(this.hovered);
            this.topPlate.material = this.resourceService.getTerritoryPlateMaterial('null');

        } else {
            this.setHovered(this.hovered);

            this.topPlate.material = this.resourceService.getTerritoryPlateMaterial(this.targetTerritory.landscapeType);

            this.model = this.resourceService.newTerrainMesh(targetTerritory.landscapeType);
            if(this.model == null) {
                this.model = this.resourceService.newIndustryMesh(
                    targetTerritory.villageType, 
                    targetTerritory.villageLevel);
            }
            if(this.model != null) {
                this.container.add(this.model);
                this.model.position.set(0, 0, 0.0003);
            }
        }
        this.pickBorderMaterial();
    }

    private pickBorderMaterial() {
        if(this.targetTerritory == null || this.targetTerritory.isOwnedByNature())
            this.border.material = this.resourceService.getBorderMaterial('none');
        else
            this.border.material = this.resourceService.getBorderMaterial
                (this.targetTerritory.owner.isCurrentPlayer ? 'owner' : 'enemy_player');
    }

    /**
     * Sets whether the tile should enabled or disabled.
     * If a tile is disabled, it isn't showed and can't be interacted with.
     * @param disabled True, if the tile should be disabled, false, if the tile should be enabled.
     */
    setDisabled(disabled: boolean) {
        if(disabled == this.disabled)
            return;
        this.disabled = disabled;

        this.applyTerritory(null);
        this.setHovered(false);

        if(disabled) {
            this.base.visible = false;
        } else {
            this.base.visible = true;
        }
    }

    /**
     * @returns True, if the is disabled, false otherwise.
     */
    isDisabled() : boolean {
        return this.disabled;
    }

    getTerritory() {
        return this.targetTerritory;
    }

    hasTerritoryLoaded() {
        return this.targetTerritory != null;
    }

    setPosition(x: number, y: number, z: number) {
        this.container.position.set(x, y, z);
    }

    setTargetZ(value: number) {
        this.targetZ = value;
    }

    /**
     * Copies the position from the given vector.
     * @param position The vector from which to copy the position.
     */
    copyPosition(position: Vector3) {
        this.container.position.copy(position);
    }

    translate(x: number, y: number) {
        this.container.position.add(new Vector3(x, y, 0));
    }

    setCoordinates(x: number, y: number) {
        
    }

    /**
     * @returns A clone of the position of the tile.
     */
    getPosition() : Vector3 {
        return this.container.position.clone().setZ(0);
    }

    update(deltaTime: number) {
        if(false && this.animationMixer != null) {
            this.animationMixer.update(deltaTime);
        }

        if(this.targetZ != this.container.position.z)
            this.updateHeight(deltaTime);
    }

    updateHeight(deltaTime: number) {
        this.container.position.setZ(
            moveTowards(this.container.position.z, this.targetZ, deltaTime));
    }

    /**
     * Sets whether the tile is hovered or not and changes it's visuals accordingly.
     * @param hovered True, if the tile is hovered over, false otherwise.
     */
    setHovered(hovered: boolean) {
        if(this.disabled && hovered)
            console.warn("Should not be hovering over a disabled tile!");

        this.hovered = hovered;
        if(this.targetTerritory == null) {
            this.topPlate.material = this.resourceService.getTerritoryPlateMaterial( hovered ? 'hoverred' : null);
        } else {
            this.topPlate.material = this.resourceService.getTerritoryPlateMaterial
                (hovered ? 'hovered' :this.targetTerritory.landscapeType );
        }
    }

    setSelected(selected: boolean) {
        if(this.disabled && selected)
            console.warn('Should not be selecting a disabled tile!');
        
        this.selected = selected;
    }
}