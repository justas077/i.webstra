import { Injectable } from '@angular/core';
import { AuthService } from './auth-service';
import { deepCopy } from '../shared/utilities';
import { Subject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class PlayerService {
    playerInformationChanged: Subject<PlayerInformation> = new Subject();

    private username: string;
    private information: PlayerInformation;

    private apiUrl: string;

    private goldUpdater;
    private troopUpdater;

    constructor(private authService: AuthService, private http: HttpClient) {
        this.apiUrl = environment.apiUrl;
        this.authService.authStateChanged.subscribe(
            (user) => {
                if(user == null) {
                    this.username = null;
                } else {
                    this.username = user.username;
                }
            }
        );
    }

    loadInformation(): Observable<PlayerInformation> {
        return this.http.get(environment.apiUrl + "user/resources").pipe(
            map((result) => {
                const info = new PlayerInformation();
                
                info.gold = result['gold'];
                info.goldHr = result['gold_per_hour'];
                info.troops = [];
                info.troopsHr = [];
                info.totalTroops = 0;
                info.totalTroopsHr = 0;

                for (var property in result['troops']) {
                    if (result['troops'].hasOwnProperty(property)) {
                        info.troops[property] = result['troops'][property];
                        info.totalTroops += info.troops[property];
                    }
                }

                for (var property in result['troops_per_hour']) {
                    if (result['troops_per_hour'].hasOwnProperty(property)) {
                        info.troopsHr[property] = result['troops_per_hour'][property];
                        info.totalTroopsHr += info.troopsHr[property];
                    }
                }

                this.information = info;
                const copy = deepCopy(this.information);
                this.playerInformationChanged.next(copy);
                return copy;
            }),
            tap( () => this.updateInfoUpdaters() )
        );
    }

    private updateInfoUpdaters() {
        clearInterval(this.goldUpdater);
        clearInterval(this.troopUpdater);

        if (this.information != null) {
            if (this.information.goldHr != 0) {
                this.goldUpdater = setInterval(() => {
                    this.information.gold++;
                    this.playerInformationChanged.next(deepCopy(this.information));
                }, (1 / this.information.goldHr) * 3600000);
            }

            // if (this.information.totalTroopsHr != 0) {
            //     this.goldUpdater = setInterval(() => {
            //         this.information.totalTroops++;
            //         this.playerInformationChanged.next(deepCopy(this.information));
            //     }, (1 / this.information.totalTroopsHr) * 3600000);
            // }
        }      
    }

    incrementTroop(troop: string) {
        this.information.troops[troop]++;
        this.information.totalTroops++;
        this.playerInformationChanged.next(deepCopy(this.information));
    }

    getInformation() : PlayerInformation {
        return deepCopy(this.information);
    }

    isLoaded(): boolean {
        return this.information != null;
    }
}

export class PlayerInformation {
    gold: number;
    goldHr: number;
    troops: { [id: string]: number }[];
    troopsHr: { [id: string]: number }[];
    totalTroops: number;
    totalTroopsHr: number;
}