export class User {
    id: number;
    username: string;
    isCurrentPlayer: boolean;
}