import { HttpHeaders } from '@angular/common/http';
import { User } from './user';
import { Subject } from 'rxjs';
import { deepCopy } from '../shared/utilities';

export class AuthService {
    authStateChanged: Subject<User> = new Subject();

    private token: string;
    private user: User;

    appendAuthHeader(headers: HttpHeaders): HttpHeaders {
        return headers.append('Authorization', "Bearer " + this.token);
    }

    createAuthHeader() {
        return new HttpHeaders({ 'Authorization': "Bearer " + this.token});
    }

    isAuthenticated(): boolean {
        return this.token != null;
    }

    signIn(user: string) {
        if(user == 'just') {
            this.token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjYyNjA0MWM0YmUwZTk0NWFhZDI3MTZiMjIzY2M2YmNmZTFiMzFlMjAwYWMxOTY1Njg1MWY2Nzc4YmJiMTE2OGU4OWVhYmY1MTk4ZGZkZDdmIn0.eyJhdWQiOiIyIiwianRpIjoiNjI2MDQxYzRiZTBlOTQ1YWFkMjcxNmIyMjNjYzZiY2ZlMWIzMWUyMDBhYzE5NjU2ODUxZjY3NzhiYmIxMTY4ZTg5ZWFiZjUxOThkZmRkN2YiLCJpYXQiOjE1NTIzNDQ2MTksIm5iZiI6MTU1MjM0NDYxOSwiZXhwIjoxNTgzOTY3MDE5LCJzdWIiOiI0Iiwic2NvcGVzIjpbIioiXX0.KQTSZhTpqGmMrmSboMI91RkVrShIoc6YJsdq97H3M1S9MHAbfAnjmBxBoMJAD-bxDnD-ZOhSMzpKTvI9bMez9ornxRwa5ho3V34Epy4VSsBsONccFryCTws06Cj653nshj0kWdp9twsSf9CJBi7GHZ9tpa7XtHSB7-sfd9B2zry6_tj2t2KU8dLcVFi52K_uCVP4TJ22qt5dp96_SPMwGS3RwXFOEc5_YT24RVJaGDN6GbXj8ditaDRaVicL0PvAfJ50VeCkZCTUuWoltjFrHk6wvsk5NMZp-zdieocKjoUqCxF59tpvg5-9Tg8AY2qY98zd4Fy3DQ6acgzKgz38IP_5L_0e3r_s8TQCgakCKKLKuFyDWIpg-rUK5a0BpTClNUrnsPMh00RrdK-kuv6h8fjjTSCoRNp8rgEy1qUD-jYY89O1TM6wYuHVfQfeimWQduMlPmnn8P0lTf9BuuK0sRaqb2YIckTXi9XU7BFpGScIv0unGQDq-i_y_EYpvclgJ8wKPn2yieeyTfCwvmOtDp2maJU9MiEJBtuNEdqodoRceonOJAsuG6YXWp4NL6H-VIgN9eBrmpdwIQrVd-Ym1HV7cNN0r2ADSgI28XRkb3XI61a4rRvGGMLUfejMO4SXdO5p50A_cTMEe6rBCDx1ehnrJ6GSqwZ-zm0p7KeeEq0";
            this.user = { id: 3, username: 'justinianas', isCurrentPlayer: true };
        } else {
            this.token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjZhMDllODU5MzUzNzA0MGY4OGM2NDczMzI5YjlhZjU4ZDBmZTQ5ODk2YjdlZjMwNTg3NzkxOTEyOWFjMzI2N2Y0MGM0ODY5MDMyNzk1MDE2In0.eyJhdWQiOiIyIiwianRpIjoiNmEwOWU4NTkzNTM3MDQwZjg4YzY0NzMzMjliOWFmNThkMGZlNDk4OTZiN2VmMzA1ODc3OTE5MTI5YWMzMjY3ZjQwYzQ4NjkwMzI3OTUwMTYiLCJpYXQiOjE1NTE4OTk1NTQsIm5iZiI6MTU1MTg5OTU1NCwiZXhwIjoxNTgzNTIxOTU0LCJzdWIiOiIzIiwic2NvcGVzIjpbIioiXX0.ddIpmv2XkIT9SaEcNs6p3JqcX4dAQeAHvwZ-KGhHoDfrdl21EGkvt5nQPgEhRPx_3oC0zE74xNbhzGiM5gQe1cLPjOkT5EOtuzeHSH6AP1tekrYHI8tsyqMBv3kb6pgwvY3ld4WALJEySFJvvogUDc37LD4wRNMES-sck6i9SzvRpqmc_-ZgN9zB8H-9Y7_dvduMgrRpj4IgJd2QpJIsVmPIaHG53fKhszbX0Y356KjRj5j58IihlYpe8OrnmCPEaamg1seDxQLfAtdoppKPDfs3ck65iIyVdk5vO-6hWynWU7kWPSTpPJq_5ZyPyVGfLqqEup4Jo5QmX0lYCnNeDfrPPTjJQ-_tfUN1hlMOmfUCfz1ugIV0YrgrRDC4X9cszeABwKeCyPEHy0yEV3NxnmuKmLk7D0z_rAkN8tuFoo4SlQgkfhI6_PdDPtBrXyW5vhDn0sUwJSQturEjDQlQZsCRAPg3KrISc9iMV56kKTOgCtUXGH8MCaIY3cGqmMcFJwrgqmcFYLmsQGhOE8XIKUwoBByQ_cK2OjINeLU0tU5RX9CyVR5BSxoQ0dlsa7VRjmc9Q9dzgPY44NEpSlms33RQ5N07zhBxgnmz5GLOIcymUr-oVrN6u6nVYLinxSTG94IhY3kgw95JAus61WWy1gVwcP_gNdweejCi8Y3BoAw";
            this.user = { id: 3, username: 'angrychild', isCurrentPlayer: true };
        }
        
        this.authStateChanged.next(deepCopy(this.user));
    }

    getUsername(): string {
        return this.user.username;
    }

    isIdOfUser(id: number): boolean{
        if(this.user == null)
            return false;
        if(id == null)
            return false;
        return id == this.user.id;
    }
}