import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth/auth-service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(private authService: AuthService, private route: ActivatedRoute, private router: Router) { 

  }

  ngOnInit() {
    
  }

  onSignInAngrychild() {
    this.authService.signIn('angry');

    if (this.route.queryParams['value']['redirectTo'] != null)
      this.router.navigate(this.route.queryParams['value']['redirectTo']);
    else
      this.router.navigate(['/']);
  }

  onSignInJustinianas() {
    this.authService.signIn('just');

    if (this.route.queryParams['value']['redirectTo'] != null)
      this.router.navigate(this.route.queryParams['value']['redirectTo']);
    else
      this.router.navigate(['/']);
  }
}
