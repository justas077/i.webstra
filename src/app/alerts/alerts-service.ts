import { Subject } from 'rxjs';

export class AlertsService {
    alertReceived: Subject<Alert> = new Subject<Alert>();

    pushSystemAlert(headerText: string, bodyText: string) {
        this.alertReceived.next(new SystemAlert(headerText, bodyText));
    }
    pushUpgradeAlert(industryName: string, coors: string) {
        this.alertReceived.next(new UpgradeAlert("UPGRADE COMPLETE", industryName, coors));
    }
    pushAttackAlert(playerName: string, coors: string) {
        this.alertReceived.next(new AttackAlert("YOU ARE UNDER ATTACK!", playerName, coors));
    }

    pushSystemAlertReloadPage(headerText: string) {
        this.pushSystemAlert(headerText, 
            'Reload the page to ensure the current state of the game is valid!');
    }
}

export class Alert {
    constructor(private headerText: string){}
}

export class SystemAlert extends Alert {
    constructor(headerText: string, private bodyText: string) { 
        super(headerText) 
    }
}

export class UpgradeAlert extends Alert {
    constructor(headerText: string, private industryName: string, private coors: string) {
        super(headerText)
    }
}

export class AttackAlert extends Alert {
    constructor(headerText: string, private playerName: string, private coors: string) {
        super(headerText)
    }
}