import { Component, OnInit, Input, HostListener, HostBinding, Output, EventEmitter, OnDestroy } from '@angular/core';
import { AlertListEntry } from '../alerts.component';
import { Subscription } from 'rxjs';
import { SystemAlert, AttackAlert, UpgradeAlert } from '../alerts-service';
import { SafeStyle } from '@angular/platform-browser';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit, OnDestroy {
  private dismissSub = Subscription.EMPTY;
  private _model: AlertListEntry;

  @Input() set model(value: AlertListEntry) {
    if (this._model != value) {
      this._model = value;
      this.modelChanged();
    }
  };
  get model(): AlertListEntry {
    return this._model;
  }

  constructor() {}
  ngOnInit() {

  }

  isSystem() {
    return this.model.alert instanceof SystemAlert;
  }

  isAttack() {
    return this.model.alert instanceof AttackAlert;
  }

  isUpgrade() {
    return this.model.alert instanceof UpgradeAlert;
  }

  @HostListener('click') 
  alertClicked() {

    this.model.tryDissmiss();
  }

  @HostBinding('class.in') public in: boolean = false;
  @HostBinding('class.out') public out: boolean = false;
  @HostBinding('style.background-color') public bgColor: SafeStyle;
  
  private modelChanged() {
    this.bgColor = this.model.alert instanceof SystemAlert 
      ? 'rgb(195, 70, 74)'
      : 'rgb(25, 42, 50)'
      
    setTimeout(() => {
      this.in = true;
    }, 100);

    this.dismissSub.unsubscribe();
    this.dismissSub = this.model.onDismissed.subscribe((entry) => {
      
      this.in = false;
      this.out = true;
    });
  }

  ngOnDestroy() {
    this.dismissSub.unsubscribe();
  }
}