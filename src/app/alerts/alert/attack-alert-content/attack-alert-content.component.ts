import { Component, OnInit, Input } from '@angular/core';
import { AttackAlert } from '../../alerts-service';

@Component({
  selector: 'app-attack-alert-content',
  templateUrl: './attack-alert-content.component.html',
  styleUrls: ['./attack-alert-content.component.css']
})
export class AttackAlertContentComponent implements OnInit {
  @Input() model: AttackAlert;
  constructor() { }

  ngOnInit() {
  }

}
