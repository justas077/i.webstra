import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttackAlertContentComponent } from './attack-alert-content.component';

describe('AttackAlertContentComponent', () => {
  let component: AttackAlertContentComponent;
  let fixture: ComponentFixture<AttackAlertContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttackAlertContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttackAlertContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
