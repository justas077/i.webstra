import { Component, OnInit, Input } from '@angular/core';
import { SystemAlert } from '../../alerts-service';

@Component({
  selector: 'app-system-alert-content',
  templateUrl: './system-alert-content.component.html',
  styleUrls: ['./system-alert-content.component.css']
})
export class SystemAlertContentComponent implements OnInit {
  @Input() model: SystemAlert;
  
  constructor() { }

  ngOnInit() {
  }

}
