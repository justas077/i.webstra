import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemAlertContentComponent } from './system-alert-content.component';

describe('SystemAlertContentComponent', () => {
  let component: SystemAlertContentComponent;
  let fixture: ComponentFixture<SystemAlertContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemAlertContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemAlertContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
