import { Component, OnInit, Input } from '@angular/core';
import { UpgradeAlert } from '../../alerts-service';

@Component({
  selector: 'app-upgrade-alert-content',
  templateUrl: './upgrade-alert-content.component.html',
  styleUrls: ['./upgrade-alert-content.component.css']
})
export class UpgradeAlertContentComponent implements OnInit {
  @Input() model: UpgradeAlert;
  constructor() { }

  ngOnInit() {
  }

}
