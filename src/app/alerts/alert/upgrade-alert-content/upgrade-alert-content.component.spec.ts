import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpgradeAlertContentComponent } from './upgrade-alert-content.component';

describe('UpgradeAlertContentComponent', () => {
  let component: UpgradeAlertContentComponent;
  let fixture: ComponentFixture<UpgradeAlertContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgradeAlertContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpgradeAlertContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
