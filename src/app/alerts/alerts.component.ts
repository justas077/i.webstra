import { Component, OnInit, OnDestroy, HostBinding, HostListener, EventEmitter, OnChanges } from '@angular/core';
import { Alert, AlertsService } from './alerts-service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit, OnDestroy {
  private alertPushSub = Subscription.EMPTY;

  private maxAlerts: number = 5;

  alerts: AlertListEntry[] = [];
  constructor(private alertsService: AlertsService) { }

  ngOnInit() {
    this.alertPushSub = this.alertsService.alertReceived.subscribe(
      (alert: Alert) => this.newAlertPushed(alert));

    // setTimeout(() => {
    //   this.alertsService.pushSystemAlert('Sup', 'press M for a random alert');
    // }, 200);
    // setTimeout(() => {
    //   this.alertsService.pushSystemAlert('Sup', 'press M for a random alert we need a lot moarmore text for anoda line');
    // }, 200);
    // setTimeout(() => {
    //   this.alertsService.pushUpgradeAlert('FARM', '23 : -456');
    // }, 200);
    // setTimeout(() => {
    //   this.alertsService.pushAttackAlert('angrychild', '23 : -456');
    // }, 200);
    // setTimeout(() => {
    //   this.alertsService.pushAttackAlert('JUSTINIANAS', '23 : -456');
    // }, 200);
  }

  @HostListener('document:keydown.M')
  ondunno() {
    const f = Math.random();
    if(f < 0.333) {
      this.alertsService.pushSystemAlert('Something happend!', 'It appears there is an error situation!');
    }
    else if (f < 0.666) {
      this.alertsService.pushUpgradeAlert('FARM', '23 : -456');
    }
    else {
      this.alertsService.pushAttackAlert('justinianas', '23 : -456');
    }
  }

  private newAlertPushed(alert: Alert) {
    const entry = new AlertListEntry(alert);

    entry.onDismissed.subscribe((entry: AlertListEntry) =>
    {
      setTimeout(() => {
        const i = this.alerts.indexOf(entry);
        if (i != -1)
          this.alerts.splice(i, 1);
      }, 500);
    });

    this.alerts.unshift(entry);

    this.ensureNotTooMany();
  }

  ensureNotTooMany() {
    if (this.alerts.length > this.maxAlerts) {
      let nonDismissed = 0;

      this.alerts.forEach(entry => {
        if (!entry.dismissed)
          nonDismissed++;
      });

      for (let i = this.alerts.length-1; i >= 0 && nonDismissed > this.maxAlerts; i--) {
        if(!this.alerts[i].dismissed) {
          this.alerts[i].tryDissmiss();
          nonDismissed--;
        }
      }
    }
  }

  ngOnDestroy() {
    this.alertPushSub.unsubscribe();
  }
}

export class AlertListEntry {

  onDismissed: EventEmitter<AlertListEntry> = new EventEmitter<AlertListEntry>();
  dismissed: boolean = false;

  constructor(public alert: Alert) { }

  /**
   * Dismissed the alert if it hasn't been dismissed yet.
   */
  tryDissmiss() {
    if(!this.dismissed) {

      this.dismissed = true;
      this.onDismissed.next(this);
      this.onDismissed.complete();
    }
  }
}