FROM nginx:alpine

WORKDIR /home/webstra-fr/i.webstra

RUN apk update
RUN apk add nodejs

COPY dist/ .
